
package pop_replay;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PoP_Replay_Util 
{
	static Integer lock = new Integer(0);
	static Integer printLock = new Integer(0);
	static HashMap<Long, String> threadNames = new HashMap<Long, String>();
	static HashMap<String, Integer> prevForked = new HashMap<String, Integer>();
	static String currentThread = new String("-1");
	static BufferedReader br;
	static int lineNo = 0;
	static String lastLine;

	static{
			try{
				br = new BufferedReader(new FileReader("global_trace"));
			}
			catch(Exception e){
				e.printStackTrace();
			}
		
	}

    public static void addThread(){
    	Thread parent = Thread.currentThread();
	    if(threadNames.size() == 0){
			threadNames.put(parent.getId(), "0");
		}
    }
    public static void addThread(Thread th){
    	Thread parent = Thread.currentThread();
    	String newThreadName = new String(threadNames.get(parent.getId()));
    	int prevForks = 0; 
    	
    	if(!prevForked.containsKey(newThreadName)){
    		prevForked.put(newThreadName, 1);
    	}
    	else{
    		prevForks = prevForked.get(newThreadName).intValue();
    		prevForked.remove(newThreadName);
    		prevForked.put(newThreadName, prevForks+1);
    	}
    	newThreadName = newThreadName + "." + prevForks;
    	
    	threadNames.put(th.getId(), newThreadName);
    }
  
    /* You can modify criticalBefore() to receive the required arguments */
    public static void criticalBefore () {
    	String threadName = threadNames.get(Thread.currentThread().getId());
    	synchronized(printLock){
    		System.out.println("\n\n======="+threadName+" critBeforeEnter============");
    		System.out.println(	"CurrentThread: " + currentThread +
    				"CurrentStatement: " + lineNo);
    	}
    	while(true){
    		
    		Thread th = Thread.currentThread();
    		synchronized (lock) {
   		
	    		if( currentThread.equals(threadName) ){
	    			break;
	    		}
 		
    		}
    	}
    	System.out.println("=======CritBefore Exit "+threadName+"=============\n\n");
    }
    
    /* You can modify criticalAfter() to receive the required arguments */
    public static void criticalAfter () {
    	synchronized(printLock){
    		System.out.println("\n\n===========CritAfter Enter"+threadNames.get(Thread.currentThread().getId())+"=================");
    		if(lastLine!=null)
    			System.out.println("LastLine Thread " +lastLine);
    		System.out.println("CriticalAfter in " + threadNames.get(Thread.currentThread().getId()));
    		System.out.println("CurrentThread " + threadNames.get(Thread.currentThread().getId()));
    	}
    	String nextLine = null;
    	try{
    		while(true){
    			nextLine = br.readLine();
    			lastLine = nextLine;
    			lineNo ++;
    			if(nextLine == null)
    				break;
    			if(nextLine.contains("Begin") || nextLine.contains("End")){
    				continue;
    			}
    			break;
    		}
    		
    		if(nextLine == null){
    			nextLine = "0, 0";
    		}
    		System.out.println("NextLine: "+nextLine);
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	synchronized(lock){
    		currentThread = nextLine.split(", ")[0];
    	}
    	System.out.println("NextThread is " + currentThread);
    	System.out.println("============Crit After Exit"+threadNames.get(Thread.currentThread().getId())+"================\n\n");
    }
       
}
