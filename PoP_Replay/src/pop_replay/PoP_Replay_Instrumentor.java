
package pop_replay;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import soot.Body;
import soot.Local;
import soot.PatchingChain;
import soot.Scene;
import soot.SceneTransformer;
import soot.SootClass;
import soot.SootMethod;
import soot.Unit;
import soot.Value;
import soot.ValueBox;
import soot.jimple.AssignStmt;
import soot.jimple.IdentityStmt;
import soot.jimple.IntConstant;
import soot.jimple.InvokeExpr;
import soot.jimple.InvokeStmt;
import soot.jimple.Jimple;
import soot.jimple.NullConstant;
import soot.jimple.ReturnStmt;
import soot.jimple.ReturnVoidStmt;
import soot.jimple.SpecialInvokeExpr;
import soot.jimple.StaticFieldRef;
import soot.jimple.StaticInvokeExpr;
import soot.jimple.Stmt;
import soot.jimple.StringConstant;
import soot.jimple.VirtualInvokeExpr;
import soot.util.Chain;

public class PoP_Replay_Instrumentor extends SceneTransformer
{
	SootMethod criticalBefore=null, criticalAfter=null,
				addThread=null,addThread2=null,sopln;
    
    @Override
    protected void internalTransform(String arg0, Map<String, String> arg1) {
    	
    	
    	criticalBefore = Scene.v().getMethod("<pop_replay.PoP_Replay_Util: void criticalBefore()>");
    	criticalAfter = Scene.v().getMethod("<pop_replay.PoP_Replay_Util: void criticalAfter()>");
    	addThread = Scene.v().getMethod("<pop_replay.PoP_Replay_Util: void addThread(java.lang.Thread)>");
    	addThread2 = Scene.v().getMethod("<pop_replay.PoP_Replay_Util: void addThread()>");
    	sopln = Scene.v().getMethod("<java.io.PrintStream: void println(java.lang.String)>");
        Chain<SootClass> allClasses = Scene.v().getApplicationClasses();
        for (SootClass curClass: allClasses) {
        
            /* These classes must be skipped */
            if (curClass.getName().contains("pop_replay.PoP_Replay_Util")
             || curClass.getName().contains("popUtil.PoP_Util")
             || curClass.getName().contains("jdk")) {
                continue;
            }
            
            List<SootMethod> allMethods = curClass.getMethods();
            for (SootMethod curMethod: allMethods) {   
            	if(curMethod.getName().contains("<init>"))
            		continue;
            	methodTransformer(curMethod);
            	System.out.println(curMethod.getActiveBody().toString());
            }  
        }  
    }
    
    void handleCritical(SootMethod sm, Unit u){
    	InvokeExpr ie = Jimple.v().newStaticInvokeExpr(criticalBefore.makeRef());
    	InvokeStmt is = Jimple.v().newInvokeStmt(ie);
    	
    	sm.getActiveBody().getUnits().insertBefore(is, u);
    	
    	ie = Jimple.v().newStaticInvokeExpr(criticalAfter.makeRef());
    	is = Jimple.v().newInvokeStmt(ie);
    	sm.getActiveBody().getUnits().insertAfter(is, u);
    	
    }
    
    
    void methodTransformer(SootMethod m){
    	Unit initial = null;
    	for(Unit u:m.getActiveBody().getUnits()){
    		if(u instanceof IdentityStmt)
    			continue;
    		else{
    			initial = u;
    			break;
    		}		
    	}
    	
    	SootClass c = Scene.v().getSootClass("java.io.PrintStream");
    	Local l = Jimple.v().newLocal("stream", c.getType());
    	m.getActiveBody().getLocals().add(l);
    	InvokeExpr ie;
    	InvokeStmt is;
    	

    	
    	
    	 VirtualInvokeExpr vvie = Jimple.v().newVirtualInvokeExpr(l,sopln.makeRef(),StringConstant.v(m.getSignature()));
    	 is = Jimple.v().newInvokeStmt(vvie);
    	m.getActiveBody().getUnits().insertAfter(is, initial);
    	
    	
    	
//	$r4 = <java.lang.System: java.io.PrintStream err>;
    	
    	m.getActiveBody().getUnits().insertAfter(Jimple.v().newAssignStmt(l, Jimple.v().newStaticFieldRef(Scene.v()
				.getField("<java.lang.System: java.io.PrintStream out>").makeRef()))
				,initial);
    	
    	Iterator<Unit> it = m.getActiveBody().getUnits().snapshotIterator();
    	while(it.hasNext()){
    		Unit u = it.next();
    		if(u instanceof AssignStmt){
    			
    			AssignStmt as = (AssignStmt)u;
    			
    			if( (as.getRightOp() instanceof StaticFieldRef 
    					&& !as.getRightOp().toString().contains("lock")
    					&& as.getRightOp().toString().contains("Main"))
    					|| (as.getLeftOp() instanceof StaticFieldRef 
    					&& !as.getLeftOp().toString().contains("lock")
    					&& as.getLeftOp().toString().contains("Main")) ){
    				handleCritical(m,u);
    			}
    			
    		}
    		else if(u instanceof InvokeStmt){
    			ie = ((InvokeStmt)u).getInvokeExpr();
    			String methodName = ie.getMethod().getName();
    			if(methodName.equals("start")){
    				VirtualInvokeExpr vie = (VirtualInvokeExpr)ie;
    				StaticInvokeExpr vieToCall = Jimple.v().
    						newStaticInvokeExpr(addThread.makeRef(),vie.getBase() );
    				is = Jimple.v().newInvokeStmt(vieToCall);
    				m.getActiveBody().getUnits().insertBefore(is, u);
    				
    			}
    			if(methodName.equals("start") || 
    					methodName.equals("join") ||
    					methodName.equals("lock") ||
    					methodName.equals("unlock") ){
    		
    				handleCritical(m,u);
    			}
    		}
    		
    	}
    	
    	if( m.getName().equals("<clinit>")){
    		StaticInvokeExpr sie = Jimple.v().newStaticInvokeExpr(criticalAfter.makeRef());
    		is = Jimple.v().newInvokeStmt(sie);
        	m.getActiveBody().getUnits().insertAfter(is, initial);
    	}
    	
    	if(m.getName().equals("main") || m.getName().equals("<clinit>")){
    		StaticInvokeExpr sie = Jimple.v().newStaticInvokeExpr(addThread2.makeRef());
    		is = Jimple.v().newInvokeStmt(sie);
        	m.getActiveBody().getUnits().insertAfter(is, initial);
    	}
    }
}
