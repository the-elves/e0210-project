package e0210.Project1;
/*
 * @author Sridhar Gopinath		-		g.sridhar53@gmail.com
 * 
 * Course project,
 * Principles of Programming Course, Fall - 2016,
 * Computer Science and Automation (CSA),
 * Indian Institute of Science (IISc),
 * Bangalore
 */


import java.util.Iterator;
import java.util.Map;

import soot.Body;
import soot.BodyTransformer;
import soot.Local;
import soot.LongType;
import soot.Modifier;
import soot.RefType;
import soot.Scene;
import soot.SootClass;
import soot.SootField;
import soot.SootMethod;
import soot.Unit;
import soot.jimple.AddExpr;
import soot.jimple.AssignStmt;
import soot.jimple.GotoStmt;
import soot.jimple.IdentityStmt;
import soot.jimple.IfStmt;
import soot.jimple.InvokeExpr;
import soot.jimple.InvokeStmt;
import soot.jimple.Jimple;
import soot.jimple.JimpleBody;
import soot.jimple.LongConstant;
import soot.jimple.ReturnStmt;
import soot.jimple.ReturnVoidStmt;
import soot.jimple.Stmt;
import soot.jimple.StringConstant;
import soot.jimple.SwitchStmt;
import soot.jimple.ThrowStmt;
import soot.util.Chain;
public class Backup_Proj1 extends BodyTransformer {
	boolean classVariableCreated=false;
	SootClass stream;
	SootMethod sopln,sopln_str;
	Local streamInstance;
	SootField classCounter;
	AssignStmt as;
	@Override
	protected void internalTransform(Body b, String phaseName, Map<String, String> options) {
		//if(b.getMethod().getName().equals("<init>"))
			//return;
		
		//Check if class variable is created, if not. Create it and initialize to zero
//		synchronized(this){
			System.out.println("Instrumenting " + b.getMethod().getSignature());
			//if(!classVariableCreated && Scene.v().getMainClass().declaresField("globalBranchInstrumentationCounter", LongType.v())){
			if(!Scene.v().getMainClass().declaresField("globalBranchInstrumentationCounter", LongType.v())){
					classCounter = new SootField ("globalBranchInstrumentationCounter",soot.LongType.v(),Modifier.STATIC|Modifier.PUBLIC);
					b.getMethod().getDeclaringClass().addField(classCounter);
					classVariableCreated= true;
					
			}else{
				classCounter = Scene.v().getMainClass().getFieldByName("globalBranchInstrumentationCounter");
			}
		
		
			stream = Scene.v().getSootClass("java.io.PrintStream");
			sopln = stream.getMethod("void print(long)");
			sopln_str = stream.getMethod("void print(java.lang.String)");
			
			
			Chain stmts = b.getUnits();
			Iterator istmt = stmts.snapshotIterator();
			Stmt placeHolder = (Stmt) istmt.next();
			
			
			
			
			//add Local variable, initialize to zero
			
			Local localCounter = Jimple.v().newLocal("lC", soot.LongType.v());
			b.getLocals().add(localCounter);
			
			while(true){
				if(!(placeHolder instanceof IdentityStmt))
					break;
				placeHolder = (Stmt) istmt.next();
			}
			
			if(b.getMethod().getName().contains("main")){
				AssignStmt as=Jimple.v().newAssignStmt(Jimple.v().newStaticFieldRef(classCounter.makeRef()),LongConstant.v(0));
				b.getUnits().insertBefore(as, placeHolder);
			}
		
			b.getUnits().insertBefore(Jimple.v().newAssignStmt(localCounter, LongConstant.v(0)), placeHolder);
			
			Local addtemp = Jimple.v().newLocal("addtmp", LongType.v());
			b.getLocals().add(addtemp);
			
			streamInstance = Jimple.v().newLocal("StreamInstance", RefType.v("java.io.PrintStream"));
			b.getLocals().add(streamInstance);
			
			b.getUnits().insertBefore(Jimple.v().newAssignStmt(streamInstance, Jimple.v().newStaticFieldRef(Scene.v()
					.getField("<java.lang.System: java.io.PrintStream out>").makeRef()))
					,placeHolder);
			
			
			
			istmt = stmts.snapshotIterator();
			
			 //= (Stmt)istmt.next();
			while(istmt.hasNext()){
				placeHolder = (Stmt)istmt.next();
				if(placeHolder instanceof IfStmt || placeHolder instanceof ThrowStmt){
					AddExpr a = Jimple.v().newAddExpr(localCounter, LongConstant.v(1));
					as = Jimple.v().newAssignStmt(localCounter, a);
					b.getUnits().insertBefore(as, placeHolder);
				}
				
				if(placeHolder instanceof ReturnStmt || placeHolder instanceof ReturnVoidStmt ){
					//adding local value to static counter
					
					{
						AssignStmt as1 = Jimple.v().newAssignStmt(addtemp, Jimple.v().newStaticFieldRef(classCounter.makeRef()));
						b.getUnits().insertBefore(as1, placeHolder);
						
						AddExpr ae = Jimple.v().newAddExpr(addtemp, localCounter);
						as1 = Jimple.v().newAssignStmt(addtemp, ae);
						b.getUnits().insertBefore(as1, placeHolder);
						
						as = Jimple.v().newAssignStmt(Jimple.v().newStaticFieldRef(classCounter.makeRef()), addtemp);
						b.getUnits().insertBefore(as, placeHolder);
						
						
						
						//print 
						
						InvokeExpr ie = Jimple.v().newVirtualInvokeExpr(streamInstance, sopln.makeRef(), localCounter);
						InvokeStmt is = Jimple.v().newInvokeStmt(ie);
						b.getUnits().insertBefore(is, placeHolder);
						
						ie = Jimple.v().newVirtualInvokeExpr(streamInstance, sopln_str.makeRef(), StringConstant.v("\n"));
						
	//					ie = Jimple.v().newVirtualInvokeExpr(streamInstance, sopln_str.makeRef(), StringConstant.v(b.getMethod().getName()+"\n"));
						is = Jimple.v().newInvokeStmt(ie);
						b.getUnits().insertBefore(is, placeHolder);
					}
					if(b.getMethod().getName().equals("main")){
						
	//					AssignStmt as1 = Jimple.v().newAssignStmt(addtemp, Jimple.v().newStaticFieldRef(classCounter.makeRef()));
	//					b.getUnits().insertBefore(as1, placeHolder);
	//					
	//					AddExpr ae = Jimple.v().newAddExpr(addtemp, localCounter);
	//					as1 = Jimple.v().newAssignStmt(addtemp, ae);
	//					b.getUnits().insertBefore(as1, placeHolder);
	//
	//					AssignStmt as = Jimple.v().newAssignStmt(Jimple.v().newStaticFieldRef(classCounter.makeRef()), addtemp);
	//					b.getUnits().insertBefore(as, placeHolder); 
						
						InvokeExpr ie;
						b.getUnits().insertBefore(Jimple.v().newAssignStmt(localCounter, Jimple.v().newStaticFieldRef(classCounter.makeRef())),
								placeHolder);
						ie = Jimple.v().newVirtualInvokeExpr(streamInstance, sopln.makeRef(), localCounter);
						InvokeStmt is = Jimple.v().newInvokeStmt(ie);
						b.getUnits().insertBefore(is, placeHolder);
	//					ie = Jimple.v().newVirtualInvokeExpr(streamInstance, sopln_str.makeRef(), StringConstant.v("\n"));
						
					}
	
					
				}
				if(placeHolder instanceof InvokeStmt){
					InvokeExpr ie = placeHolder.getInvokeExpr();
					SootMethod stmMethod = ie.getMethod();
					if(stmMethod.getSignature().equals("<java.lang.System: void exit(int)>")){
	//					print local counter
						ie = Jimple.v().newVirtualInvokeExpr(streamInstance, sopln.makeRef(), localCounter);
						InvokeStmt is = Jimple.v().newInvokeStmt(ie);
						b.getUnits().insertBefore(is, placeHolder);
						
	//					ie = Jimple.v().newVirtualInvokeExpr(streamInstance, sopln_str.makeRef(), StringConstant.v("\n"));
						ie = Jimple.v().newVirtualInvokeExpr(streamInstance, sopln_str.makeRef(), StringConstant.v("\n"));
						is = Jimple.v().newInvokeStmt(ie);
						b.getUnits().insertBefore(is, placeHolder);
						
	//					print static counter;
						AssignStmt as1 = Jimple.v().newAssignStmt(addtemp, Jimple.v().newStaticFieldRef(classCounter.makeRef()));
						b.getUnits().insertBefore(as1, placeHolder);
						
						AddExpr ae = Jimple.v().newAddExpr(addtemp, localCounter);
						as1 = Jimple.v().newAssignStmt(addtemp, ae);
						b.getUnits().insertBefore(as1, placeHolder);
	
						as = Jimple.v().newAssignStmt(Jimple.v().newStaticFieldRef(classCounter.makeRef()), addtemp);
						b.getUnits().insertBefore(as, placeHolder); 
	
						
						b.getUnits().insertBefore(Jimple.v().newAssignStmt(localCounter, Jimple.v().newStaticFieldRef(classCounter.makeRef())),
								placeHolder);
						ie = Jimple.v().newVirtualInvokeExpr(streamInstance, sopln.makeRef(), localCounter);
						is = Jimple.v().newInvokeStmt(ie);
						b.getUnits().insertBefore(is, placeHolder);
					}
	
				}
						
					
			}
	
			
			
			
			//scan thru statements
			//if branch add one to local
			
			//if last return or returnvoid print;
			//add the local variable to class variable
			//print the value of local variable.
			
			
			
			//print the modified jimple code
			System.out.println("==========================================================");
			System.out.println(b.toString());
			System.out.println("==========================================================");
			return;
		}
	//}

}
//if(b.getMethod().getName().equals("foo")){
//	//System.out.println("inside if");
//	Chain stmts = b.getUnits();
//	Iterator istmt = stmts.iterator();
//	Stmt placeHolder = (Stmt)istmt.next();
//	while(istmt.hasNext()){
//		placeHolder = (Stmt)istmt.next();
//	}
//	
//	InvokeExpr foocall = Jimple.v().newVirtualInvokeExpr(b.getThisLocal(), bar.makeRef());
//	Stmt foocallstmt = Jimple.v().newInvokeStmt(foocall);
//	stmts.insertBefore(foocallstmt,placeHolder);
//}
//System.out.println("Instrumenting Method "+b.getMethod().getSignature());