package e0210;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
/**
 * if(){
 * 	System.out.println();
 * }
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;




public class TupleDataStore {
	static HashMap<String, List<Tuple> > outputs = new HashMap<String, List<Tuple> >();
	static HashMap<Long, ThreadData> absThread = new HashMap<Long, ThreadData>();
	static HashMap<String, HashMap<String, Integer> > functionTracker = new HashMap<String, HashMap<String, Integer> >();
		
	public synchronized static int functionCalled(String functionName){
		boolean debug=false;
		long threadId = Thread.currentThread().getId();
		String tName = absThread.get(threadId).threadName;
		HashMap<String, Integer> funCounter = functionTracker.get(tName);

		if(debug){
			System.out.println(funCounter.get(functionName).toString());
		}
		
		if(funCounter.containsKey(functionName)){
			Integer count = funCounter.get(functionName);
			funCounter.remove(functionName);
			count = count + 1;
			funCounter.put(functionName,count);
			return count-1;
		}
		else{
			funCounter.put(functionName, 1	);
			return 0;
		}
		
	}
	
	
	public synchronized static void addThread(long threadId){
		boolean aTDebug=true;
		String newThreadName = new String() ;
		long parentThreadId = Thread.currentThread().getId();
		if(absThread.containsKey(threadId))
			return;
		if(absThread.containsKey(parentThreadId)){
			ThreadData t = absThread.get(parentThreadId);
			int spawned = t.numSpawned;
			t.numSpawned = t.numSpawned +1;
			newThreadName = t.threadName + "." + Integer.toString(spawned);
			absThread.put(threadId, new ThreadData(newThreadName));
		}
		else{
			newThreadName ="0";
			ThreadData t = new ThreadData("0");
			t.numSpawned = 0;
			absThread.put(threadId, t);
		}
		functionTracker.put(newThreadName,new HashMap<String, Integer>());
		if(aTDebug){
		 	System.out.println(absThread.get(threadId).threadName+"thread Added");
		}
		try {
			new FileOutputStream("ThreadOutputs/_" + newThreadName, false).close();;
		} catch (Exception e) {
			System.err.println("File Creation Failed for " + newThreadName);
		}
		
	}
	
	
	
	public synchronized static void print(long BLId, String methodSig,int wp, int pc ){
		long threadId = Thread.currentThread().getId();
		String threadName = absThread.get(threadId).threadName;
		System.out.println("printing " +BLId + " " + methodSig + "from " + threadName);
		
		Tuple t;
		if(wp==1){
			t= new Tuple(methodSig, threadName, pc, BLId);
		}
		else{
			t= new Tuple(methodSig, threadName, -1, BLId);
		}
		if(outputs.containsKey(threadName)){
			List<Tuple> list = outputs.get(threadName);			
			list.add(t);
		}
		else{
			List<Tuple> list = new ArrayList<Tuple> ();
			list.add(t);
			outputs.put(threadName, list);
		}

	}
	
	public synchronized static void print(long BLId, String methodSig,int wp  ){
		long threadId = Thread.currentThread().getId();
		String threadName = absThread.get(threadId).threadName;
		System.out.println("printing " +BLId + " " + methodSig + "from " + threadName);
		int pc;
		Tuple t;
		if(wp==1){
			pc = functionTracker.get(threadName).get(methodSig);
			t= new Tuple(methodSig, threadName, pc-1, BLId);
		}
		else{
			t= new Tuple(methodSig, threadName, -1, BLId);
		}
		if(outputs.containsKey(threadName)){
			List<Tuple> list = outputs.get(threadName);			
			list.add(t);
		}
		else{
			List<Tuple> list = new ArrayList<Tuple> ();
			list.add(t);
			outputs.put(threadName, list);
		}

	}
	
	public synchronized  static void printDump(){
		Thread t = new Thread(new Runnable() {
			
			@Override
			public void run() {
				
				System.out.println("ThreadRunning");
				for(Long l:absThread.keySet()){
					System.out.println("Thread "+absThread.get(l).threadName);
				}
				int i = 0;
				for(String s:outputs.keySet()){
					try {
						FileOutputStream f = new FileOutputStream("ThreadOutputs/_"+s);
						BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(f));
//						bw.write(outputs.get(s).toString());
						for(Tuple t:outputs.get(s)){
							bw.write(t.toString());
							System.out.println((i++)+t.toString());
							bw.newLine();
						}
						bw.close();
						f.close();
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
			}
		});
		Runtime.getRuntime().addShutdownHook(t);
		
	}
	
	public synchronized  static Tuple getTupleFromLine(String line){
		
		line=line.substring(0,line.length()-1);
		String componenets[] = line.split("\\|");
		
		
		Tuple t  = new Tuple(componenets[0].trim(),
					componenets[1].trim(),
					new Long(componenets[2]).longValue(),
					0);
		String str = componenets[3].substring(1, componenets[3].length());
		String arr[] = str.split(", ");
		t.path = new ArrayList<Integer>();
		for(String s : arr){
			if(s.equals(""))
				continue;
			t.path.add(new Integer(s.trim()));
		}
		return t;
	}
	
	static Tuple augmentTuple(Tuple t1, Tuple t2){
		if(t1 == null)
			return t2;
		Tuple t = new Tuple();
		System.out.println("exception");
		t.methodSignature=t1.methodSignature;
		t.path.addAll(t1.path);
		t.path.addAll(t2.path);
		t.previouslyCalled=t2.previouslyCalled;
		t.threadId=t1.threadId;
		return t;
	}
	
	public synchronized  static void populateTuples(){
		File dir = new File("ThreadOutputs");
		File files[] = dir.listFiles();
		outputs = new HashMap<String,List<Tuple>>();
		for(File f:files){
			try {
				BufferedReader br = new BufferedReader(
						new InputStreamReader(
								new FileInputStream("ThreadOutputs/"+f.getName())));
				String threadName = f.getName().replace('_', Character.MIN_VALUE).trim();
				String line;
				Tuple t=new Tuple();
				t.path = new ArrayList<Integer>();
				outputs.put(threadName, new ArrayList<Tuple>());
				while((line = br.readLine())!=null && !(line.equals(""))){
					System.out.println(line);
					Tuple partial = getTupleFromLine(line);
					if(partial.previouslyCalled==-1){
						t.path.addAll(partial.path);
						continue;
					}
					t.path.addAll(partial.path);
					t.methodSignature = partial.methodSignature;
					t.threadId = partial.threadId;
					t.previouslyCalled = partial.previouslyCalled;
					outputs.get(threadName).add(t);
					System.out.println("PT : " + t);
					t=new Tuple();
					t.path= new ArrayList();
					
				}
					
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
}
