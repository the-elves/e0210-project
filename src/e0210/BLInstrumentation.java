package e0210;
/*
 * @author Sridhar Gopinath		-		g.sridhar53@gmail.com

 * 
 * Course project,
 * Principles of Programming Course, Fall - 2016,
 * Computer Science and Automation (CSA),
 * Indian Institute of Science (IISc),
 * Bangalore
 */

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
/**
 *TODO think about switch
 *TODO think loops
 */
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;
import org.jgrapht.traverse.TopologicalOrderIterator;

import soot.Body;
import soot.IntType;
import soot.IntegerType;
import soot.Local;
import soot.PatchingChain;
import soot.RefType;
import soot.Scene;
import soot.SootClass;
import soot.SootField;
import soot.SootMethod;
import soot.Unit;
import soot.jimple.AddExpr;
import soot.jimple.AssignStmt;
import soot.jimple.IdentityStmt;
import soot.jimple.IntConstant;
import soot.jimple.InvokeExpr;
import soot.jimple.InvokeStmt;
import soot.jimple.Jimple;
import soot.jimple.JimpleBody;
import soot.jimple.LongConstant;
import soot.jimple.ReturnStmt;
import soot.jimple.ReturnVoidStmt;
import soot.jimple.Stmt;
import soot.jimple.StringConstant;
import soot.toolkits.graph.Block;
import soot.toolkits.graph.ExceptionalBlockGraph;
import soot.util.Chain;
import soot.util.HashChain;
public class BLInstrumentation {
	boolean classVariableCreated=false;
	SootClass streamClass;
	SootMethod sopln,sopln_str;
	Local streamInstance;
	SootField classCounter;
	AssignStmt as;
	ArrayList<Integer> loopSourceNodes;
	ArrayList<Integer> loopDestNodes;
	static long blNo=0;
	HashMap<Integer, Path> mapping = new HashMap<Integer,Path>();
	static ArrayList<Long> blRanges = new ArrayList<Long>();
	
	SootMethod print,functionCalled,printDump;
	
	//implement class for loop info
	
	BLInstrumentation(SootMethod printMethod,
			SootMethod fc,
			SootMethod pd){
		print = printMethod;
		functionCalled = fc;
		printDump=pd;
		
	}
	
	class BlockEdge extends DefaultEdge{
		int weight;
		public BlockEdge(int weight){
			setWeight(weight);
		}
		public int getWeight() {
			return weight;
		}
		public void setWeight(int weight) {
			this.weight = weight;
		}
		public String toString(){
			BlockVertex src=(BlockVertex)getSource();
			BlockVertex dest=(BlockVertex)getTarget();
			return ("\n"+"(" + Integer.toString(src.getId()) +","+Integer.toString(dest.getId())+ ","+Integer.toString(weight) + ")" + "\n");
		}
	}
	
	class BlockVertex{
		int id, label;
		public BlockVertex(int id, int label){
			setId(id);
			setLabel(label);
		}
		public int getLabel() {
			return label;
		}
		public void setLabel(int label) {
			this.label = label;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		
		public String toString(){
			return ("(" + Integer.toString(id) + ","+Integer.toString(label) + ")");
		}
	}
	
	public Block getBlockWithId(int id, ExceptionalBlockGraph cfg){
		boolean blockFound=false;
		Block currentBlock=null;
		for(Block bl:cfg.getBlocks())
			if(bl.getIndexInMethod() == id){
				blockFound = true;
				currentBlock = bl;
				break;
		}
		if (!blockFound)
			try {
				throw (new Exception());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return currentBlock;
		
	}
	
	
	
	public BlockVertex getBlockVertexWithId(int id, SimpleDirectedWeightedGraph<BlockVertex, BlockEdge> cfg){
		boolean blockFound=false;
		BlockVertex currentBlock=null;
		for(BlockVertex bl:cfg.vertexSet())
			if(bl.getId() == id){
				blockFound = true;
				currentBlock = bl;
				break;
		}
		if (!blockFound)
			try {
				throw (new Exception("BlockNotFoound"));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return currentBlock;
		
	}
	
	boolean blockExits(Block b){
		
		Queue<Unit> currentUnits = new LinkedList<Unit>();
		currentUnits.add(b.getHead());
		while(!currentUnits.isEmpty()){
			Unit currentUnit = currentUnits.remove();
			if(currentUnit instanceof InvokeStmt){
				InvokeStmt stmt = (InvokeStmt) currentUnit;
				if ( stmt.getInvokeExpr().getMethod().getName().equals("exit") ){
					//System.out.println(b.getIndexInMethod() + " exits");
					return true;
				}
			}
			
		}
		
		return false;
	}
	
	boolean isLoop(Block src,Block dest,ExceptionalBlockGraph cfg){
		int i =0;
		for(Integer ls : loopSourceNodes){
			if(ls == src.getIndexInMethod() && 
					loopDestNodes.get(i) == dest.getIndexInMethod())
				return true;
			i++;
		}
		return false;
		
	}
	
	public void addDummySrcAndDest(SimpleDirectedWeightedGraph<BlockVertex, BlockEdge> graph){
		BlockVertex head=null;
		ArrayList<BlockVertex> tails=new ArrayList<BlockVertex>();
		for(BlockVertex b : graph.vertexSet()){
			if(graph.inDegreeOf(b) == 0)
				head = b;
			else if(graph.outDegreeOf(b) == 0)
					tails.add(b);
		}
		BlockVertex dummysrc = new BlockVertex(-1, 0);
		BlockVertex dummydest = new BlockVertex(-2, 0);		
		graph.addVertex(dummysrc);
		graph.addVertex(dummydest);
		
		graph.addEdge(dummysrc, head, new BlockEdge(0));
		for(BlockVertex b : tails){
			graph.addEdge(b, dummydest,new BlockEdge(0));
		}
		
	}
	
	void addDummyEdges(Queue<Integer> srcNodes, Queue<Integer> destNodes, 
						SimpleDirectedWeightedGraph<BlockVertex, BlockEdge> convertedGraph){
		BlockVertex start = getBlockVertexWithId(-1, convertedGraph);
		BlockVertex sink = getBlockVertexWithId(-2, convertedGraph);
		for(Integer srcid:srcNodes){
			int destid = destNodes.remove();
			BlockVertex loopStart = getBlockVertexWithId(srcid, convertedGraph);
			BlockVertex loopEnd = getBlockVertexWithId(destid, convertedGraph);
			convertedGraph.addEdge(start, loopEnd,new BlockEdge(0));
			boolean alreadyConnected = false;
			for(BlockEdge e: convertedGraph.outgoingEdgesOf(loopStart)){
				if(convertedGraph.getEdgeTarget(e).getId() == -2){
					alreadyConnected = true;
				}
			}
			if(!alreadyConnected){
				convertedGraph.addEdge(loopStart, sink, new BlockEdge(0));
			}
		}
	}
	
	protected SimpleDirectedWeightedGraph<BlockVertex,BlockEdge> 
		convertSootToJGraphT(ExceptionalBlockGraph cfg){
		boolean cstjDebug = false;
		if(cstjDebug){
			System.out.println("BlockGraph = "+ cfg.toString());
		}
		SimpleDirectedWeightedGraph<BlockVertex,BlockEdge> convertedGraph = new SimpleDirectedWeightedGraph<BlockVertex,BlockEdge>(BlockEdge.class);
		int id = 0;
		for(Block bl : cfg.getBlocks()){
			convertedGraph.addVertex( new BlockVertex(bl.getIndexInMethod(), 0) );
		}
		ArrayList<Integer> visitedNodes = new ArrayList<Integer>();
		Deque<Integer> currentNodes = new LinkedList<Integer>();
		for(Block bl: cfg.getHeads()){
			currentNodes.add(bl.getIndexInMethod());
			visitedNodes.add(bl.getIndexInMethod());
		}
		Integer nextChild;
		while(!currentNodes.isEmpty()){
			Integer currentNode;		
			currentNode = currentNodes.peekLast();
			if(cstjDebug){
				System.out.println("CurrentNode = " + currentNode);
			}
			if(cstjDebug){
				System.out.println("Stack = " + currentNodes.toString());
			}
			visitedNodes.add(currentNode);
			Block currentBlock = getBlockWithId(currentNode, cfg);
			if(blockExits(currentBlock)){
				currentNodes.removeLast();
				continue;
			}
			nextChild = getNotVisitedChild(getBlockWithId(currentNode,cfg),cfg,visitedNodes);
			if(cstjDebug){
				System.out.println("Next Child = "+ nextChild);
			}
			if( nextChild == null){
				for(Block bl : currentBlock.getSuccs()){
					if(currentNodes.contains(bl.getIndexInMethod())){
						//backedge
						if(cstjDebug){
							System.out.println("Loop Detected" + currentNode +"to"+bl.getIndexInMethod());
						}
						loopSourceNodes.add(currentBlock.getIndexInMethod());
						loopDestNodes.add(bl.getIndexInMethod());
					}
					else if(!convertedGraph.containsEdge(getBlockVertexWithId(currentNode, convertedGraph),
							getBlockVertexWithId(bl.getIndexInMethod(), convertedGraph))){
						BlockVertex src,dest;
						src = getBlockVertexWithId(currentNode, convertedGraph);
						dest = getBlockVertexWithId(bl.getIndexInMethod(), convertedGraph);
						convertedGraph.addEdge(src, dest,new BlockEdge(0));				
					}
				} 
				
				currentNodes.removeLast();
			}
			else{
				Block destBlock = getBlockWithId(nextChild,cfg);
			
				//Not backedge
				BlockVertex src,dest;
				src = getBlockVertexWithId(currentNode, convertedGraph);
				dest = getBlockVertexWithId(nextChild, convertedGraph);
				convertedGraph.addEdge(src, dest,new BlockEdge(0));
				currentNodes.add(nextChild);
			}			
			
		}
		//backedge
		if(cstjDebug){
			System.out.println("Loop Source" + loopSourceNodes.toString());
			System.out.println("Loop Source" + loopDestNodes.toString());
			
		}
		
		if(loopSourceNodes.size()!=0){
			addDummySrcAndDest(convertedGraph);
			addDummyEdges(new LinkedList<Integer>(loopSourceNodes),new LinkedList<Integer>(loopDestNodes),convertedGraph);
		}
		
		return convertedGraph;
		
	}

	Integer getNotVisitedChild(Block b,ExceptionalBlockGraph cfg, ArrayList<Integer> visitedNodes) {
		List<Block> children = cfg.getSuccsOf(b);
		for(Block bl : children){
			if (!visitedNodes.contains(bl.getIndexInMethod()))
				return bl.getIndexInMethod();
		}
		return null;
	}



	public BlockVertex getVertexWithId(int id,SimpleDirectedWeightedGraph<BlockVertex, BlockEdge> graph){
		
		for(BlockVertex b:graph.vertexSet())
			if(b.getId() == id)
				return b;
		return null;
	}
	

	public void setEdgeWeights(SimpleDirectedWeightedGraph<BlockVertex, BlockEdge> graph){
			TopologicalOrderIterator<BlockVertex, BlockEdge> tI = new TopologicalOrderIterator<>(graph);
			Deque<BlockVertex> stack = new LinkedList<BlockVertex>();
			while(tI.hasNext())
				stack.addLast(tI.next());
			//if tail set lable 1
			while(!stack.isEmpty()){	
				BlockVertex currentVertex =  stack.removeLast();
//				System.out.println("Current Vertex = " + currentVertex.getId() );
				//if tail
				if(graph.outDegreeOf(currentVertex) == 0)
					currentVertex.setLabel(1);
				else{
					//iterate over all the children and instrument edges
					
					//1.generate set of children
					int parentLabel=0;
					for(BlockEdge childEdge:graph.outgoingEdgesOf(currentVertex)){
//						System.out.println("                   considering" + graph.getEdgeTarget(childEdge).getId() + "Label = " + graph.getEdgeTarget(childEdge).getLabel());
						childEdge.setWeight(parentLabel);
						parentLabel=parentLabel+graph.getEdgeTarget(childEdge).getLabel();
						currentVertex.setLabel(parentLabel);
					}
					
				}
			}
	}
	
	boolean wholePath(Path p,ExceptionalBlockGraph cfg){
		List<Integer> heads = new ArrayList<Integer>();
		List<Integer> tails = new ArrayList<Integer>();
			
		
		for(Block bl:cfg.getHeads())
			heads.add(bl.getIndexInMethod());
		for(Block bl:cfg.getTails())
			tails.add(bl.getIndexInMethod());
		
//		System.out.println("path"+p.toString());
//		System.out.println("head"+heads.toString());
//		System.out.println("teal"+tails.toString());
		if(p.size()>0){
			if(p.get(0)!=-1){
				return true;
			}
			else{
				if(heads.contains(p.get(1)) && tails.contains(p.get(p.size()-2)))
					return true;
			}
		}
		return false;
		
	}
	
	void writeMappingToFile(Body b,ExceptionalBlockGraph cfg) throws FileNotFoundException, IOException{
		
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("mapping.ser",true)));
		
		bw.write("MethodStart\n");
		bw.write(b.getMethod().getSignature()+"\n");
		bw.write("LoopPathStart\n");
		for(Integer i:mapping.keySet()){
			Path path = mapping.get(i);
			if( !wholePath(path,cfg)){
				String toWrite = path.toString();
				toWrite = toWrite.replace("[", "").
						replace("]", "").
						replace(",", " ");
				toWrite = i.toString() +":" + toWrite;
				bw.write(toWrite);
				bw.newLine();
			}
			
		}
		bw.write("LoopPathEnd\n");
		
		bw.write("WholePathStart\n");
		for(Integer i:mapping.keySet()){
			Path path = mapping.get(i);
		
			if( wholePath(path,cfg)){
				String toWrite = path.toString();
				toWrite = toWrite.replace("[", "").
						replace("]", "").
						replace(",", " ");
				toWrite = i.toString() +":" + toWrite;
				bw.write(toWrite);
				bw.newLine();
			
		}
			
			
		}
		bw.write("WholePathEnd\n");
		bw.write("MethodEnd\n");
		bw.close();
	}
	class Path extends ArrayList<Integer>{}
	
	/**
	 * Data structure 
	 * hashmap of 
	 * @param graph Instrumented Graph 
	 * @return
	 */
	public HashMap<Integer, Path > calculateBLPaths(SimpleDirectedWeightedGraph<BlockVertex,BlockEdge> graph){
		HashMap<Integer, Path > mapping = new HashMap<Integer, Path >();
		
		ArrayList<ArrayList <Integer> > allPaths = new ArrayList<ArrayList <Integer> >();
		
		HashMap<Integer, ArrayList<Path> > tempPaths= new HashMap<Integer, ArrayList<Path> >();
		TopologicalOrderIterator<BlockVertex, BlockEdge> tI = new TopologicalOrderIterator<>(graph);
		Deque<BlockVertex> stack = new LinkedList<BlockVertex>();
		
		
			
		if(graph.vertexSet().size()==1){
			mapping.put(0, new Path());
			mapping.get(0).add(0);
			return mapping;
		}
			
			
		//end}} 
		
		while(tI.hasNext())
			stack.addLast(tI.next());
		
		//for every vertex in reverse topological order
		while(!stack.isEmpty()){
			//if tail
			BlockVertex currentVertex = stack.removeLast();
			if(graph.outDegreeOf(currentVertex)==0){
				Path temp = new Path();
				if(tempPaths.get(currentVertex.getId()) == null)
					tempPaths.put(currentVertex.getId(),new ArrayList<Path>());
				temp.add(currentVertex.getId());
				tempPaths.get(currentVertex.getId()).add(temp);
			}
			else{
				ArrayList<Path> emanatingFrom;
				for(BlockEdge be:graph.outgoingEdgesOf(currentVertex)){
					
					BlockVertex toSearch = graph.getEdgeTarget(be);
					
					emanatingFrom = tempPaths.get(toSearch.getId());
					
					for(Path a:emanatingFrom){
						//TODO check order
						Path temp = new Path();
						for(Integer tocopy : a){
							temp.add(tocopy);
						}
						
						temp.add(currentVertex.getId());
						
						if(!tempPaths.containsKey(currentVertex.getId())){
							tempPaths.put(currentVertex.getId(), new ArrayList<Path>());
						}
						
						(tempPaths.get(currentVertex.getId())).add(temp);
					}
					
				}
				
			}
		}
		for(Integer i:tempPaths.keySet()){
			for(Path p:tempPaths.get(i)){
				Collections.reverse(p);
			}
		}
		
		BlockVertex src=null;
		for(BlockVertex b: graph.vertexSet()){
			if(graph.inDegreeOf(b)==0)
				src = b;
		}
		
		assert(src!=null);
		ArrayList<Path> completePaths = tempPaths.get(src.getId());
		for(Path p:completePaths){
			int id = idForPath(p, graph);
			mapping.put(id, p);
		}
		return mapping;		
	}
	
	
	int getWeight(SimpleDirectedWeightedGraph<BlockVertex, BlockEdge> graph,
			int src, 
			  int dest){
		
		BlockVertex bvSrc,bvDest;
		bvSrc=getBlockVertexWithId(src, graph);
		bvDest=getBlockVertexWithId(dest, graph);
		for(BlockEdge e:graph.outgoingEdgesOf(bvSrc)){
			if(graph.getEdgeTarget(e).id == dest){
				return e.getWeight();
			}
		}
		return -1;
	}
	
	boolean isExitFunctionCall(Unit stmt){
		if(stmt instanceof InvokeStmt)
			return ( ((InvokeStmt)stmt).getInvokeExpr().getMethod().getName().equals("exit") );
		else return false;
	}
	
	Chain<Unit> getExitPoints(Body b){
		JimpleBody jb = (JimpleBody)b;
		Chain<Unit> stmts = jb.getUnits();
		Chain<Unit> toInstrument = new PatchingChain<Unit>(new HashChain<Unit>());
		for(Unit u:stmts){
			Stmt stmt = (Stmt)u;
			if(stmt instanceof ReturnStmt || stmt instanceof ReturnVoidStmt)
				toInstrument.add(u);
			else if(stmt instanceof InvokeStmt){
				if( ((InvokeStmt)stmt).getInvokeExpr().getMethod().getName().equals("exit") ){
					toInstrument.add(u);
				}
			}
		}
		return toInstrument;
	}
	
	
	void instrumentCode(Body b, SimpleDirectedWeightedGraph<BlockVertex,BlockEdge> graph){
		JimpleBody jb = (JimpleBody)b;
		Local id = Jimple.v().newLocal("blPathIdentifier", soot.LongType.v());
		b.getLocals().add(id);
		AssignStmt asStmt = Jimple.v().newAssignStmt(id, LongConstant.v(blNo));
		Unit toinsert=b.getUnits().getFirst();
		
		for(Unit u:b.getUnits()){
			System.out.println("BLLOOP\t" + u.toString());
			if(u instanceof IdentityStmt){
				toinsert = u;
			}
			else
				break;
		}
		
		if(b.getMethod().getName().equals("<clinit>") || b.getMethod().getName().equals("main")){
			for(Unit u:b.getUnits()){
				
				if(u instanceof InvokeStmt){
					if( ((InvokeStmt)u).getInvokeExpr().getMethod().getName().equals("addThread") ){
						toinsert = u;
						break;
					}
				}
			}
		}
		
		b.getUnits().insertAfter(asStmt, toinsert);
		
		
		
		
		Local funCall = Jimple.v().newLocal("previouslyCalled", IntType.v());
		b.getLocals().add(funCall);
		InvokeExpr ie = Jimple.v().newStaticInvokeExpr(functionCalled.makeRef(),
				StringConstant.v(b.getMethod().getSignature()));
		AssignStmt as = Jimple.v().newAssignStmt(funCall, ie);
		b.getUnits().insertAfter(as, toinsert);
		
		ExceptionalBlockGraph cfg = new ExceptionalBlockGraph(b);
		for(Block bl : cfg.getBlocks()){
			for(Block succ:bl.getSuccs()){
				if(isLoop(bl, succ, cfg)){
					int j = getWeight(graph, -1, succ.getIndexInMethod());
					int i=  getWeight(graph, bl.getIndexInMethod(),-2);
					List<Unit> toinstrument = new ArrayList<Unit>();
					AddExpr ae = Jimple.v().newAddExpr(id, LongConstant.v(i));
					as = Jimple.v().newAssignStmt(id, ae);
					toinstrument.add(as);
					String s = b.getMethod().getSignature();
					ie = Jimple.v().newStaticInvokeExpr(print.makeRef(),id,StringConstant.v(s),IntConstant.v(0),IntConstant.v(-1));
					InvokeStmt is = Jimple.v().newInvokeStmt(ie);
					toinstrument.add(is);
					as = Jimple.v().newAssignStmt(id, LongConstant.v(j));
					toinstrument.add(as);					
					ae = Jimple.v().newAddExpr(id, LongConstant.v(blNo));
					as = Jimple.v().newAssignStmt(id, ae);
					toinstrument.add(as);
					b.getUnits().insertOnEdge(toinstrument, bl.getTail(), succ.getHead());
				}else{
					int toAdd = getWeight(graph, bl.getIndexInMethod(), succ.getIndexInMethod());
					AddExpr ae = Jimple.v().newAddExpr(id, LongConstant.v(toAdd));
					AssignStmt toInsert = Jimple.v().newAssignStmt(id, ae);	
					b.getUnits().insertOnEdge(toInsert, bl.getTail(), succ.getHead());
				}
			}
		}
		
		Chain<Unit> pointsToInstrument = getExitPoints(b);
		for(Unit lastUnit : pointsToInstrument){
			//print()
				String s = b.getMethod().getSignature();
				
				ie = Jimple.v().newStaticInvokeExpr(print.makeRef(),id,StringConstant.v(s),IntConstant.v(1),funCall);
				InvokeStmt is = Jimple.v().newInvokeStmt(ie);
				b.getUnits().insertBefore(is, lastUnit);
				
				if(b.getMethod().getName().equals("main") || isExitFunctionCall(lastUnit)){
					ie = Jimple.v().newStaticInvokeExpr(printDump.makeRef());
					is = Jimple.v().newInvokeStmt(ie);
					b.getUnits().insertBefore(is, lastUnit);
				
			}
			
		}
	}
	
	void addLocalHashToGlobal(HashMap<Integer, Path> tm){
		mapping = new HashMap<Integer,Path>();
		for(Integer key : tm.keySet()){
			mapping.put((int)(key+blNo), tm.get(key));
		}
	}
	
	
	public int idForPath(Path p, SimpleDirectedWeightedGraph<BlockVertex, BlockEdge> graph){
		int id=0;
		int i=0;
		for(Integer vertexId:p){
			BlockVertex thisVertex = getVertexWithId(vertexId, graph);
			for(BlockEdge e:graph.outgoingEdgesOf(thisVertex)){
				if(graph.getEdgeTarget(e).getId() == p.get(i+1)){
					id=id+e.getWeight();
					break;
				}
			}
			if(i==p.size()-2){
				break;
			}
			else
				i++;
			
		}
		return id;
	}
		
	protected synchronized void internalTransform (Body b) {
		if(b.getMethod().getName().equals("<init>")){
			if(b.getMethod().getName().contains("clinit"))
				System.out.println(b.toString());
			else
				System.out.println("Skipping Init");
			return;
		}
		if(blNo==0){
			try {
				new FileOutputStream("mapping.ser",false).close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		ExceptionalBlockGraph cfg = new ExceptionalBlockGraph(b);
		boolean debug=true;
		streamClass = Scene.v().getSootClass("java.io.PrintStream");
		streamInstance = Jimple.v().newLocal("StreamInstance", RefType.v("java.io.PrintStream"));
		
		
		Unit lastUnit= ((JimpleBody)b).getFirstNonIdentityStmt();
		
		b.getLocals().add(streamInstance);
		
		sopln = Scene.v().getMethod("<java.io.PrintStream: void println(long)>");
		
		
		b.getUnits().insertBefore(Jimple.v().newAssignStmt(streamInstance, Jimple.v().newStaticFieldRef(Scene.v()
				.getField("<java.lang.System: java.io.PrintStream out>").makeRef()))
				,lastUnit);
		
		
		
			loopDestNodes = new ArrayList<Integer>();
			loopSourceNodes = new ArrayList<Integer>();
			if(debug){
				System.out.println("cfg");
				System.out.println(cfg.toString());
				
				System.out.println(b.toString());
				
				System.out.println("Original Graph");
				System.out.println(cfg.toString());
			}
		
		SimpleDirectedWeightedGraph<BlockVertex, BlockEdge> convertedGraph = convertSootToJGraphT(cfg);
		if(debug){
			
			System.out.println("converted Graph");
			System.out.println(convertedGraph.toString());
					
		}
		
		setEdgeWeights(convertedGraph);
		if(debug){
			System.out.println("Edge Weights");
			System.out.println(convertedGraph.toString());
			System.out.println("Loop source " + loopSourceNodes.toString());
			System.out.println("Loop dest " + loopDestNodes.toString());
			for(BlockEdge e: convertedGraph.edgeSet()){
				System.out.println(e.toString());
			}
		}
		
				
		HashMap<Integer, Path> tempMapping = calculateBLPaths(convertedGraph);
		System.out.println("Temp mapping ================******\n"+tempMapping.toString());
		addLocalHashToGlobal(tempMapping);
		instrumentCode(b, convertedGraph);
		blNo = blNo + tempMapping.size();
		blRanges.add(blNo);		
		if(debug){
			System.out.println("mapping " +mapping.toString());
		}			
		
		try {
			writeMappingToFile(b,cfg);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("==========================Final Body==============================");
		System.out.println(b.toString());
	}

	
}
