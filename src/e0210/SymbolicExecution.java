package e0210;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.microsoft.z3.ArithExpr;
import com.microsoft.z3.BitVecExpr;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Context;
import com.microsoft.z3.Expr;
import com.microsoft.z3.IntExpr;
import com.microsoft.z3.IntNum;
import com.microsoft.z3.Model;
import com.microsoft.z3.Solver;
import com.microsoft.z3.Status;

import soot.Body;
import soot.Immediate;
import soot.Local;
import soot.Scene;
import soot.SceneTransformer;
import soot.SootClass;
import soot.SootField;
import soot.SootMethod;
import soot.Unit;
import soot.Value;
import soot.jimple.AddExpr;
import soot.jimple.AssignStmt;
import soot.jimple.BinopExpr;
import soot.jimple.ConditionExpr;
import soot.jimple.DivExpr;
import soot.jimple.IfStmt;
import soot.jimple.InterfaceInvokeExpr;
import soot.jimple.InvokeExpr;
import soot.jimple.InvokeStmt;
import soot.jimple.MulExpr;
import soot.jimple.RemExpr;
import soot.jimple.ReturnStmt;
import soot.jimple.ReturnVoidStmt;
import soot.jimple.SpecialInvokeExpr;
import soot.jimple.StaticFieldRef;
import soot.jimple.StaticInvokeExpr;
import soot.jimple.Stmt;
import soot.jimple.SubExpr;
import soot.jimple.VirtualInvokeExpr;
import soot.jimple.XorExpr;
import soot.toolkits.graph.Block;
import soot.toolkits.graph.ExceptionalBlockGraph;
import soot.util.Chain;

public class SymbolicExecution extends SceneTransformer {

	class ForkJoinTuple {
		String TID, childTID;
		boolean fork;
		String po;
	}

	class ReadWriteTuple {
		String TID;
		boolean read;
		String variableName;
		String symBolicValue;
		String po;
	}

	class LockUnlockTuple {
		String TID;
		boolean lock;
		String lockObject;
		String po;
		String lockSurName;
	}

	class Statement {
		String TID;
		String statement;
	}

	class AbsUnit {
		Unit u;
		Tuple t;
		StackFrame sf;
	}

	class StackFrame {
		Unit currentUnit;
		Block block;
		int blockIndex;
		Tuple tuple;
		ExceptionalBlockGraph cfg;

		public StackFrame(Unit cu, Tuple t, Block b) {
			currentUnit = cu;
			tuple = t;
			block = b;
			blockIndex = 0;
		}

		StackFrame() {
		}

		StackFrame(StackFrame s) {
			currentUnit = s.currentUnit;
			block = s.block;
			blockIndex = s.blockIndex;
			tuple = s.tuple;
			cfg = s.cfg;

		}
	}

	class BeginEndTuple {
		String po, correspondingPo;
		String TID;
		boolean begin;
	}

	int callNumber;
	Chain<SootClass> sootClasses;
	String currentPo, previousPo;
	int threadEvent;
	HashMap<String, Unit> pos;
	HashMap<String, Integer> previousFork;
	HashMap<String, Integer> previousCalls;
	HashMap<String, Integer> symNames;
	HashMap<String, HashMap<String, String>> localToThredId;
	HashMap<String, HashMap<String, String>> localToLock;

	List<ReadWriteTuple> readWriteTuples;
	List<ForkJoinTuple> forkJoinTuples;
	List<LockUnlockTuple> lockUnlockTuples;
	HashMap<String, Statement> statements;
	List<BeginEndTuple> bETuples;

	String currentThreadName;
	Deque<StackFrame> stackTrace;
	Deque<String> funNameStack;
	Deque<HashMap<String, Integer>> symNameStack;

	List<String> skipList;
	List<String> sharedVariables;
	HashMap<String, List<String>> localVariables;
	HashMap<String, String> lastProgramOrder;

	TupleDataStore tds;
	List<String> constrains;
	List<String> poConstraints;
	List<String> pathConstraints;
	List<String> assignConstrains;
	List<String> programOrders;
	List<String> intVars;
	HashMap<String, Integer> evaluatedProgramOrders;

	void initialize() {
		// skiplist;
		skipList = new ArrayList<String>();
		skipList.add("ReadWriteTuple");
		skipList.add("StackFrame");
		skipList.add("LockUnlockTuple");
		skipList.add("ForkJoinTuple");

		callNumber = 0;
		sootClasses = Scene.v().getApplicationClasses();
		currentPo = "O_0_0";
		pos = new HashMap<String, Unit>();
		symNames = new HashMap<String, Integer>();
		readWriteTuples = new ArrayList<ReadWriteTuple>();
		forkJoinTuples = new ArrayList<ForkJoinTuple>();
		lockUnlockTuples = new ArrayList<LockUnlockTuple>();
		currentThreadName = "0";
		sharedVariables = new ArrayList<String>();
		localVariables = new HashMap<String, List<String>>();
		previousFork = new HashMap<String, Integer>();
		localToThredId = new HashMap<String, HashMap<String, String>>();

		localToLock = new HashMap<String, HashMap<String, String>>();
		poConstraints = new ArrayList<String>();
		constrains = new ArrayList<String>();
		pathConstraints = new ArrayList<String>();
		assignConstrains = new ArrayList<String>();
		programOrders = new ArrayList<String>();
		intVars = new ArrayList<String>();
		lastProgramOrder = new HashMap<String, String>();
		evaluatedProgramOrders = new HashMap<String, Integer>();
		statements = new HashMap<String, Statement>();
		bETuples = new ArrayList<BeginEndTuple>();
		symNameStack = new LinkedList<HashMap<String, Integer>>();
	}

	Tuple getRunMethodTuple(List<Tuple> list) {
		for (Tuple l : list) {
			if (l.methodSignature.contains("void run()") && l.previouslyCalled == 0)
				return l;
		}
		return null;
	}

	Tuple getMainMethodTuple(List<Tuple> list) {
		for (Tuple l : list) {
			System.out.println(l.methodSignature);
			if ((l.methodSignature.contains("void main(java.lang.String[])")) && l.previouslyCalled == 0)
				return l;
		}
		return null;
	}

	void addToBlockStack(List<Integer> blocks, Deque blockStack) {
		Collections.reverse(blocks);
		for (Integer i : blocks) {
			blockStack.add(i);
		}
	}

	Block getBlockWithId(ExceptionalBlockGraph cfg, int id) {
		List<Block> list = cfg.getBlocks();
		for (Block b : list) {
			if (b.getIndexInMethod() == id)
				return b;
		}
		return null;
	}

	String symRead(Local l) {
		String toreturn = l.getName();
		if (symNames.containsKey(l.getName())) {
			toreturn = toreturn + "_" + symNames.get(l.getName()).toString();
			return toreturn;
		} else {
			symNames.put(l.getName(), 0);
			return l.getName() + "_" + "0";

		}
	}

	String symRead(String field) {
		String toreturn = field;
		if (symNames.containsKey(field)) {
			toreturn = toreturn + "_" + symNames.get(field).toString();
			return toreturn;
		} else {
			symNames.put(field, 0);
			return field + "_" + "0";

		}
	}

	String symWrite(String field) {
		String toreturn = new String(field);
		if (symNames.containsKey(field)) {
			Integer curVersion = symNames.get(field);
			symNames.remove(field);
			symNames.put(field, curVersion + 1);
			toreturn = toreturn + "_" + symNames.get(field).toString();
			return toreturn;
		} else {
			symNames.put(field, 0);
			return field + "_" + "0";
		}
	}

	String symWrite(Local l) {
		String toreturn = l.getName();
		if (symNames.containsKey(l.getName())) {
			Integer curVersion = symNames.get(l.getName());
			symNames.remove(l.getName());
			symNames.put(l.getName(), curVersion + 1);
			toreturn = toreturn + "_" + symNames.get(l.getName()).toString();
			return toreturn;
		} else {
			symNames.put(l.getName(), 0);
			return l.getName() + "_" + "0";
		}
	}

	void handleAssignStmt(AssignStmt as, Unit u, Tuple t) {
		System.out.println("Handing assign");
		if (as.getRightOp() instanceof AddExpr) {
			AddExpr ae = (AddExpr) as.getRightOp();
			if (ae.getOp1() instanceof Local && ae.getOp2() instanceof Local) {
				Local r1 = (Local) ae.getOp1();
				Local r2 = (Local) ae.getOp2();
				Local l = (Local) as.getLeftOp();
				String constraint = symRead(r1) + "+" + symRead(r2);
				constraint = symWrite(l) + "=" + constraint;
				assignConstrains.add(constraint);
				System.out.println(constraint);
			} else if (ae.getOp1() instanceof Immediate && ae.getOp2() instanceof Local) {
				String r1 = ((Immediate) ae.getOp1()).toString();
				Local r2 = (Local) ae.getOp2();
				Local l = (Local) as.getLeftOp();
				String constraint = r1 + "+" + symRead(r2);
				constraint = symWrite(l) + "=" + constraint;
				assignConstrains.add(constraint);
				System.out.println(constraint);
			} else if (ae.getOp1() instanceof Local && ae.getOp2() instanceof Immediate) {
				Local r1 = (Local) ae.getOp1();
				String r2 = ((Immediate) ae.getOp2()).toString();
				Local l = (Local) as.getLeftOp();
				String constraint = symRead(r1) + "+" + r2;
				constraint = symWrite(l) + "=" + constraint;
				assignConstrains.add(constraint);
				System.out.println(constraint);
			}
		} else if (as.getRightOp() instanceof SubExpr) {
			SubExpr ae = (SubExpr) as.getRightOp();
			if (ae.getOp1() instanceof Local && ae.getOp2() instanceof Local) {
				Local r1 = (Local) ae.getOp1();
				Local r2 = (Local) ae.getOp2();
				Local l = (Local) as.getLeftOp();
				String constraint = symWrite(l) + "=" + symRead(r1) + "-" + symRead(r2);
				assignConstrains.add(constraint);
				System.out.println(constraint);
			} else if (ae.getOp1() instanceof Immediate && ae.getOp2() instanceof Local) {
				String r1 = ((Immediate) ae.getOp1()).toString();
				Local r2 = (Local) ae.getOp2();
				Local l = (Local) as.getLeftOp();
				String constraint = symWrite(l) + "=" + r1 + "-" + symRead(r2);
				assignConstrains.add(constraint);
				System.out.println(constraint);
			} else if (ae.getOp1() instanceof Local && ae.getOp2() instanceof Immediate) {
				Local r1 = (Local) ae.getOp1();
				String r2 = ((Immediate) ae.getOp2()).toString();
				Local l = (Local) as.getLeftOp();
				String constraint = symWrite(l) + "=" + symRead(r1) + "-" + r2;
				assignConstrains.add(constraint);
				System.out.println(constraint);
			}
		} else if (as.getRightOp() instanceof MulExpr) {
			MulExpr ae = (MulExpr) as.getRightOp();
			if (ae.getOp1() instanceof Local && ae.getOp2() instanceof Local) {
				Local r1 = (Local) ae.getOp1();
				Local r2 = (Local) ae.getOp2();
				Local l = (Local) as.getLeftOp();
				String constraint = symWrite(l) + "=" + symRead(r1) + "*" + symRead(r2);
				assignConstrains.add(constraint);
				System.out.println(constraint);
			} else if (ae.getOp1() instanceof Immediate && ae.getOp2() instanceof Local) {
				String r1 = ((Immediate) ae.getOp1()).toString();
				Local r2 = (Local) ae.getOp2();
				Local l = (Local) as.getLeftOp();
				String constraint = symWrite(l) + "=" + r1 + "*" + symRead(r2);
				assignConstrains.add(constraint);
				System.out.println(constraint);
			} else if (ae.getOp1() instanceof Local && ae.getOp2() instanceof Immediate) {
				Local r1 = (Local) ae.getOp1();
				String r2 = ((Immediate) ae.getOp2()).toString();
				Local l = (Local) as.getLeftOp();
				String constraint = symWrite(l) + "=" + symRead(r1) + "*" + r2;
				assignConstrains.add(constraint);
				System.out.println(constraint);
			}
		} else if (as.getRightOp() instanceof DivExpr) {
			DivExpr ae = (DivExpr) as.getRightOp();
			if (ae.getOp1() instanceof Local && ae.getOp2() instanceof Local) {
				Local r1 = (Local) ae.getOp1();
				Local r2 = (Local) ae.getOp2();
				Local l = (Local) as.getLeftOp();
				String constraint = symWrite(l) + "=" + symRead(r1) + "/" + symRead(r2);
				assignConstrains.add(constraint);
				System.out.println(constraint);
			} else if (ae.getOp1() instanceof Immediate && ae.getOp2() instanceof Local) {
				String r1 = ((Immediate) ae.getOp1()).toString();
				Local r2 = (Local) ae.getOp2();
				Local l = (Local) as.getLeftOp();
				String constraint = symWrite(l) + "=" + r1 + "/" + symRead(r2);
				assignConstrains.add(constraint);
				System.out.println(constraint);
			} else if (ae.getOp1() instanceof Local && ae.getOp2() instanceof Immediate) {
				Local r1 = (Local) ae.getOp1();
				String r2 = ((Immediate) ae.getOp2()).toString();
				Local l = (Local) as.getLeftOp();
				String constraint = symWrite(l) + "=" + symRead(r1) + "/" + r2;
				assignConstrains.add(constraint);
				System.out.println(constraint);
			}
		} else if (as.getRightOp() instanceof RemExpr) {
			BinopExpr be = (BinopExpr) as.getRightOp();
			String cons = "";
			if (be.getOp1() instanceof Local && be.getOp2() instanceof Local) {
				cons = symRead(((Local) be.getOp1())) + " % " + symRead((Local) be.getOp2());
			} else if (be.getOp1() instanceof Immediate && be.getOp2() instanceof Local) {
				cons = ((Immediate) be.getOp1()).toString() + " % " + symRead((Local) be.getOp2());
			} else if (be.getOp2() instanceof Immediate && be.getOp1() instanceof Local) {
				cons = symRead((Local) be.getOp1()) + " % " + ((Immediate) be.getOp2()).toString();
			}
			String ac = symWrite((Local) as.getLeftOp()) + " = " + cons;
			assignConstrains.add(ac);
			System.out.println(ac);
		} else if (as.getRightOp() instanceof XorExpr) {
			BinopExpr be = (BinopExpr) as.getRightOp();
			String cons = "";
			if (be.getOp1() instanceof Local && be.getOp2() instanceof Local) {
				cons = symRead(((Local) be.getOp1())) + " ^ " + symRead((Local) be.getOp2());
			} else if (be.getOp1() instanceof Immediate && be.getOp2() instanceof Local) {
				cons = ((Immediate) be.getOp1()).toString() + " ^ " + symRead((Local) be.getOp2());
			} else if (be.getOp2() instanceof Immediate && be.getOp1() instanceof Local) {
				cons = symRead((Local) be.getOp1()) + " ^ " + ((Immediate) be.getOp2()).toString();
			}
			String ac = symWrite((Local) as.getLeftOp()) + " = " + cons;
			assignConstrains.add(ac);
			System.out.println(ac);
		} else if (as.getRightOp() instanceof StaticFieldRef) {

			ReadWriteTuple rw = new ReadWriteTuple();
			StaticFieldRef field = (StaticFieldRef) as.getRightOp();
			System.out.println(field.getType().toString());
			if (field.getType().toString().equals("java.util.concurrent.locks.Lock")) {
				localToLock.get(t.methodSignature).put(((Local) as.getLeftOp()).getName(),
						field.getField().getSignature());
				System.out.println("Skipped Assignment");
			} else if (field.getType().toString().equals("java.io.PrintStream")) {
				System.out.println("Skipped StreamAssignment");
			} else {
				rw.TID = new String(t.threadId);
				rw.read = true;
				rw.symBolicValue = symWrite(field.getField().getSignature());
				rw.variableName = field.getField().getSignature();
				rw.po = currentPo;
				String sym1 = rw.symBolicValue;
				readWriteTuples.add(rw);
				String ac = symWrite((Local) as.getLeftOp()) + "=" + sym1;
				assignConstrains.add(ac);
				System.out.println(ac);
			}
		} else if (as.getLeftOp() instanceof StaticFieldRef) {
			SootField sf = ((StaticFieldRef) as.getLeftOp()).getField();
			ReadWriteTuple rw = new ReadWriteTuple();
			rw.read = false;
			rw.po = currentPo;
			rw.variableName = sf.getSignature();
			// rw.symBolicValue = symWrite(sf.getSignature());
			rw.TID = new String(t.threadId);
			String constraint = rw.symBolicValue + "Read";
			String ac = null;
			if (as.getRightOp() instanceof Local) {
				ac = symWrite(sf.getSignature()) + " = " + symRead((Local) as.getRightOp());
				rw.symBolicValue = symRead((Local) as.getRightOp());
			} else if (as.getRightOp() instanceof Immediate) {
				ac = symWrite(sf.getSignature()) + " = " + ((Immediate) as.getRightOp()).toString();
				rw.symBolicValue = ((Immediate) as.getRightOp()).toString();
			}
			readWriteTuples.add(rw);
			// assignConstrains.add(ac);
			System.out.println(ac);
		} else if (as.getLeftOp() instanceof Local && as.getRightOp() instanceof Local) {
			assignConstrains.add(symWrite((Local) as.getLeftOp()) + "=" + symRead((Local) as.getRightOp()));
		} else if (as.getLeftOp() instanceof Local && as.getRightOp() instanceof Immediate) {
			assignConstrains.add(symWrite((Local) as.getLeftOp()) + "=" + ((Immediate) as.getRightOp()).toString());
		} else if (as.getRightOp() instanceof InvokeExpr) {
			System.out.println("invoke");
			String cons = "";
			if (as.getRightOp() instanceof StaticInvokeExpr) {
				System.out.println("Staticinvoke");
				StaticInvokeExpr sie = (StaticInvokeExpr) as.getRightOp();
				SootMethod sm = sie.getMethod();
				if (sm.getName().equals("valueOf")) {
					cons = symWrite((Local) as.getLeftOp());
					if (isNumeric(sie.getArg(0).toString())) {
						cons = cons + "=" + sie.getArg(0).toString();
					}else {
						cons = cons + "=" + symRead(sie.getArg(0).toString());
					}
					assignConstrains.add(cons);
					System.out.println(cons);
				}
			} else if (as.getRightOp() instanceof VirtualInvokeExpr) {
				System.out.println("VirtualInvoke");
				VirtualInvokeExpr vie = (VirtualInvokeExpr) as.getRightOp();
				SootMethod sm = vie.getMethod();
				if (sm.getName().equals("intValue")) {
					cons = symWrite((Local) as.getLeftOp());
					cons = cons + "=" + symRead(((Local) vie.getBase()).getName());
					assignConstrains.add(cons);
					System.out.println(cons);
				} else if (sm.getName().equals("booleanValue")) {
					cons = symWrite((Local) as.getLeftOp());
					cons = cons + "=" + symRead(((Local) vie.getBase()).getName());
					assignConstrains.add(cons);
					System.out.println(cons);
				} else {
					cons = symWrite((Local) as.getLeftOp());
					cons = cons + "=" + symWrite(funNameStack.peek());
					assignConstrains.add(cons);
					System.out.println(cons);
				}
			} else if (as.getRightOp() instanceof SpecialInvokeExpr) {
				SpecialInvokeExpr sie = (SpecialInvokeExpr) as.getRightOp();
				if (!sie.getMethod().getName().contains("<init>")) {
					cons = symWrite((Local) as.getLeftOp());
					cons = cons + "=" + symWrite(funNameStack.peek());
					assignConstrains.add(cons);
					System.out.println(cons);
				}
			}
		}
		// add read write constraint

	}

	Stmt getNextStmtExecuted(IfStmt is, Tuple t, StackFrame s) {
		System.out.println(
				is.toString() + "PathBlockIndex" + s.blockIndex + " BlockMethod " + s.block.getIndexInMethod());
		System.out.println(t.path.toString());
		int nextBlockId = t.path.get(s.blockIndex + 1);
		return (Stmt) getBlockWithId(s.cfg, nextBlockId).getHead();
	}

	void handleIfStmt(IfStmt is, Unit u, Tuple t, StackFrame s) {
		// add path constrant
		System.out.println("Handing if");
		Stmt target = is.getTarget();
		Stmt actual = getNextStmtExecuted(is, t, s);
		ConditionExpr ce = (ConditionExpr) is.getCondition();

		if (target == actual) {
			Value l = ce.getOp1();
			Value r = ce.getOp2();
			if (l instanceof Local && r instanceof Local) {
				String symbol = ce.getSymbol();
				pathConstraints.add("Yes " + "" + symRead((Local) l) + symbol + symRead((Local) r));
			} else if (l instanceof Local && r instanceof Immediate) {
				String symbol = ce.getSymbol();
				String str = "Yes " + symRead((Local) l) + symbol + r.toString();
				System.out.println(str);
				pathConstraints.add(str);
			} else if (r instanceof Local && l instanceof Immediate) {
				String symbol = ce.getSymbol();
				pathConstraints.add("Yes " + l.toString() + symbol + symRead((Local) r));
			} else if (is.getCondition() instanceof Local) {
				pathConstraints.add("Yes " + symRead((Local) is.getCondition()));
			}

		} else {
			Value l = ce.getOp1();
			Value r = ce.getOp2();
			if (l instanceof Local && r instanceof Local) {
				String symbol = ce.getSymbol();
				pathConstraints.add("not " + symRead((Local) l) + symbol + symRead((Local) r));
			} else if (l instanceof Local && r instanceof Immediate) {
				String symbol = ce.getSymbol();
				String cons = "not " + symRead((Local) l) + symbol + r.toString();
				System.out.println("path **********" + cons);
				pathConstraints.add(cons);
			} else if (r instanceof Local && l instanceof Immediate) {
				String symbol = ce.getSymbol();
				pathConstraints.add("not " + l.toString() + symbol + symRead((Local) r));
			} else if (is.getCondition() instanceof Local) {
				pathConstraints.add("not " + symRead((Local) is.getCondition()));
			}
		}
	}

	void handleLocks(InvokeStmt is, Unit u, Tuple t) {
		System.out.println("Handing Locks");
		SootMethod m = is.getInvokeExpr().getMethod();
		InterfaceInvokeExpr vie = (InterfaceInvokeExpr) is.getInvokeExpr();
		LockUnlockTuple tup = new LockUnlockTuple();
		tup.po = currentPo;
		tup.TID = new String(t.threadId);
		tup.lockObject = localToLock.get(t.methodSignature).get(((Local) vie.getBase()).getName());
		if (m.getName().equals("lock"))
			tup.lock = true;
		else
			tup.lock = false;
		lockUnlockTuples.add(tup);
		if (tup.lock == true) {
			constrains.add("lock " + tup.lockObject);
		} else {
			constrains.add("unlock " + tup.lockObject);
		}
		// lockUnlockTuples.add(tup);

	}

	void handleForkJoin(InvokeStmt is, Tuple t) {
		ForkJoinTuple forkJoinTuple = new ForkJoinTuple();
		forkJoinTuple.po = currentPo;
		BeginEndTuple be = new BeginEndTuple();
		forkJoinTuple.TID = new String(t.threadId);
		VirtualInvokeExpr vie = (VirtualInvokeExpr) is.getInvokeExpr();
		SootMethod sm = vie.getMethod();
		String childId = new String(t.threadId);
		if (!previousFork.containsKey(t.threadId)) {
			forkJoinTuple.childTID = t.threadId + "." + "0";
			previousFork.put(t.threadId, new Integer(0));
		} else
			forkJoinTuple.childTID = childId + "." + Integer.toString(previousFork.get(t.threadId));
		if (sm.getName().equals("start")) {
			be.TID = forkJoinTuple.childTID;
			be.correspondingPo = currentPo;
			be.po = currentPo + "B";
			be.begin = true;
			bETuples.add(be);

			be = new BeginEndTuple();
			be.TID = forkJoinTuple.childTID;
			be.po = currentPo + "E";
			be.begin = false;
			bETuples.add(be);

			forkJoinTuple.fork = true;
			int i = previousFork.get(t.threadId).intValue();
			i = i + 1;
			previousFork.remove(t.threadId);
			previousFork.put(t.threadId, new Integer(i));
			Local l = (Local) vie.getBase();
			localToThredId.get(t.methodSignature).put(l.getName(), forkJoinTuple.childTID);
			constrains.add("Fork " + t.threadId + "->" + forkJoinTuple.childTID);
		} else if (sm.getName().equals("join")) {
			Local l = (Local) vie.getBase();
			forkJoinTuple.childTID = new String(localToThredId.get(t.methodSignature).get(l.getName()));
			forkJoinTuple.fork = false;
			constrains.add("Join " + t.threadId + "->" + forkJoinTuple.childTID);
		}
		forkJoinTuples.add(forkJoinTuple);

	}

	void handleInvokeStmt(InvokeStmt is, Unit u, Tuple t, StackFrame s) {
		// lock unlock
		// function call
		// fork join
		boolean b_break = false;
		System.out.println("Handing invoke");
		InvokeExpr ie = is.getInvokeExpr();
		SootMethod m = ie.getMethod();
		String methodName = m.getName();
		if (methodName.equals("lock") || methodName.equals("unlock")) {
			handleLocks(is, u, t);
		} else if (methodName.equals("start") || methodName.equals("join")) {
			handleForkJoin(is, t);
		} else if (m.getDeclaringClass().toString().contains("Main") && !m.getSignature().contains("<init>")
				&& !m.getSignature().contains("start")) {
			funNameStack.push(m.getSignature());
			callNumber++;
		}

	}

	void updateLastProgramOrder(Tuple t) {
		if (lastProgramOrder.containsKey(t.threadId)) {
			lastProgramOrder.put(t.threadId, currentPo);
		} else {
			lastProgramOrder.remove(t.threadId);
			lastProgramOrder.put(t.threadId, currentPo);
		}
	}

	void addStatement(Tuple t, Unit u, String po) {
		Statement s = new Statement();
		s.TID = t.threadId;
		s.statement = u.toString();
		statements.put(po, s);
	}

	boolean handleUnit(Unit u, Tuple t, StackFrame sf) {
		System.out.println("\n\nHanding unit : " + u.toString() + "\n\n");
		Stmt s = (Stmt) u;
		if (u.toString().contains("popUtil")) {
			System.out.println("Poputil skip");
			return false;
		}
		boolean breakLooping = false;

		if (threadEvent == 0) {
			programOrders.add(currentPo);
			addStatement(t, u, currentPo);
			previousPo = currentPo;
			threadEvent++;
			System.out.println("first previous " + previousPo);
		} else {
			System.out.println("previous " + previousPo);
			previousPo = currentPo;
			currentPo = "O" + "_" + t.threadId + "_" + Integer.toString(threadEvent);
			programOrders.add(currentPo);
			addStatement(t, u, currentPo);
			threadEvent++;
			constrains.add(previousPo + " < " + currentPo);
			poConstraints.add(previousPo + " < " + currentPo);
			// TODO add program order Constraint
		}

		updateLastProgramOrder(t);

		if (s instanceof AssignStmt) {
			breakLooping = true;
			AssignStmt as = (AssignStmt) s;
			handleAssignStmt(as, u, t);
		} else if (s instanceof IfStmt) {
			handleIfStmt((IfStmt) s, u, t, sf);
		} else if (s instanceof InvokeStmt) {
			handleInvokeStmt((InvokeStmt) s, u, t, sf);
		} else if (s instanceof ReturnStmt) {
			ReturnStmt rs = (ReturnStmt) s;
			if (rs.getOp() instanceof Local) {
				Local l = (Local) rs.getOp();
				String cons = symRead(funNameStack.peek()) + "=" + symRead(l);
				assignConstrains.add(cons);
			} else if (rs.getOp() instanceof Immediate) {
				String cons = symRead(funNameStack.peek()) + "=" + rs.getOp().toString();
				assignConstrains.add(cons);
			}
			funNameStack.pop();
		} else if (s instanceof ReturnVoidStmt) {
			funNameStack.pop();
		}
		// else if(s instanceof SwitchStmt){
		//
		// }
		// else if(s instanceof GotoStmt){
		//
		// }
		// else if(s instanceof IdentityStmt){
		//
		// }

		return false;
	}

	void loadSharedVariables() {
		SootClass mc = Scene.v().getMainClass();
		for (SootField f : mc.getFields()) {
			sharedVariables.add(f.getName());
		}

	}

	void loadLocalVariables(SootMethod m) {
		if (!localVariables.containsKey((m.getSignature())))
			localVariables.put(m.getSignature(), new ArrayList<String>());
		ArrayList al = (ArrayList) localVariables.get(m.getSignature());
		for (Local l : m.getActiveBody().getLocals()) {
			al.add(l.getName());
		}
	}

	void handleClInit(TupleDataStore tds) {
		funNameStack = new LinkedList<String>();
		tds.populateTuples();
		List<Tuple> l = tds.outputs.get("0");
		for (Tuple t : l) {
			if (t.methodSignature.contains("<clinit>")) {
				funNameStack.push(t.methodSignature);
				System.out.println("In clinint");
				SootMethod m = Scene.v().getMethod(t.methodSignature);
				for (Unit u : m.getActiveBody().getUnits()) {
					handleUnit(u, t,
							new StackFrame(u, t, getBlockWithId(new ExceptionalBlockGraph(m.getActiveBody()), 0)));
				}
				break;
			}
		}

	}

	boolean checkIfTail(Unit u, ExceptionalBlockGraph cfg) {
		for (Block b : cfg.getBlocks()) {
			if (u == b.getTail()) {
				return true;
			}
		}
		return false;
	}

	Unit updateStackFrame(StackFrame sf, Tuple t) {
		Iterator<Unit> unitit = sf.block.getBody().getUnits().iterator();
		Boolean last = false;

		while (unitit.hasNext()) {
			if (sf.currentUnit == unitit.next()) {
				last = !unitit.hasNext();
				break;
			}

		}
		if (last)
			return null;
		else {
			if (checkIfTail(sf.currentUnit, sf.cfg)) {
				sf.blockIndex = sf.blockIndex + 1;
				SootMethod sm = Scene.v().getMethod(t.methodSignature);
				ExceptionalBlockGraph cfg = new ExceptionalBlockGraph(sm.getActiveBody());
				Block nextBlock = getBlockWithId(cfg, t.path.get(sf.blockIndex));
				sf.block = nextBlock;
				sf.currentUnit = nextBlock.getHead();
				return nextBlock.getHead();
			} else {
				Unit temp = unitit.next();
				;
				// System.out.println("USF ::" + temp.toString());
				sf.currentUnit = temp;
				return sf.currentUnit;
			}
		}
	}

	Tuple getTuple(String tid, String sig, int pc) {
		List<Tuple> list = tds.outputs.get(tid);
		System.out.println("GT " + tid);
		System.out.println("GT " + sig);
		System.out.println("GT " + pc);
		for (Tuple t : list) {
			// System.out.println("Searching "+t.toString());
			if (t.methodSignature.equals(sig) && t.previouslyCalled == pc)
				return t;
		}
		return null;
	}

	void wait(int t) {
		try {
			Thread.sleep(t);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	List<AbsUnit> generateIntraTraceForThread(String s) {
		List units = new ArrayList<AbsUnit>();
		Tuple startingTuple, currentTuple = null;

		funNameStack = new LinkedList<String>();
		if (s.equals("0")) {
			System.out.println(s + ":inmain");
			startingTuple = getMainMethodTuple(tds.outputs.get(s));
		} else {
			startingTuple = getRunMethodTuple(tds.outputs.get(s));
		}
		funNameStack.push(startingTuple.methodSignature);
		stackTrace = new LinkedList<StackFrame>();
		previousCalls = new HashMap<String, Integer>();

		StackFrame sf = new StackFrame();
		sf.tuple = startingTuple;
		SootMethod sm = Scene.v().getMethod(startingTuple.methodSignature);
		ExceptionalBlockGraph cfg = new ExceptionalBlockGraph(sm.getActiveBody());
		sf.blockIndex = 0;
		sf.block = getBlockWithId(cfg, sf.tuple.path.get(sf.blockIndex));
		sf.currentUnit = sf.block.getHead();
		sf.cfg = cfg;
		stackTrace = new LinkedList<StackFrame>();
		stackTrace.push(sf);
		Unit currentUnit;
		while (!stackTrace.isEmpty()) {
			// wait(1000);
			StackFrame cursf = stackTrace.peekFirst();
			currentUnit = cursf.currentUnit;
			// System.out.println("\nGOT"+currentUnit.toString());
			currentTuple = cursf.tuple;

			if (!localToThredId.containsKey(currentTuple.methodSignature)) {
				localToThredId.put(currentTuple.methodSignature, new HashMap<String, String>());
			}
			if (!localToLock.containsKey(currentTuple.methodSignature)) {
				localToLock.put(currentTuple.methodSignature, new HashMap<String, String>());
			}

			if (currentUnit instanceof InvokeStmt) {
				InvokeExpr ie = ((InvokeStmt) currentUnit).getInvokeExpr();
				SootMethod cm = ie.getMethod();
				AbsUnit cu = new AbsUnit();
				cu.u = currentUnit;
				cu.t = new Tuple(currentTuple);
				cu.sf = new StackFrame(cursf);
				units.add(cu);
				if (cm.getDeclaringClass().toString().contains("Main") && !cm.getSignature().contains("<init>")
						&& !cm.getSignature().contains("start")) {
					StackFrame nsf = new StackFrame();
					Tuple t;
					int pc = 0;
					String sig;
					if (ie instanceof VirtualInvokeExpr) {
						String l = ((VirtualInvokeExpr) ie).getBase().getType().toString();

						sig = "<" + l + ":" + cm.getSignature().split(":")[1];
					} else {
						sig = cm.getSignature();
					}
					cm = Scene.v().getMethod(sig);
					if (previousCalls.containsKey(sig)) {
						pc = previousCalls.get(cm.getSignature()).intValue();
						previousCalls.remove(sig);
						previousCalls.put(sig, pc + 1);
					} else {
						previousCalls.put(sig, 1);
					}
					System.out.println("GENERATE TRACE" + cm.getSignature());

					t = getTuple(currentTuple.threadId, sig, pc);

					updateStackFrame(cursf, currentTuple);

					nsf.tuple = t;
					nsf.blockIndex = 0;
					cfg = new ExceptionalBlockGraph(cm.getActiveBody());
					nsf.block = getBlockWithId(cfg, nsf.tuple.path.get(nsf.blockIndex));
					nsf.currentUnit = nsf.block.getHead();
					nsf.cfg = cfg;
					System.out.println("NSF CU" + nsf.currentUnit);
					stackTrace.push(nsf);

				} else {
					Unit nextUnit = updateStackFrame(cursf, currentTuple);
					if (nextUnit == null)
						stackTrace.pop();
				}
			} else {
				AbsUnit cu = new AbsUnit();
				cu.sf = new StackFrame(cursf);
				cu.t = new Tuple(currentTuple);
				cu.u = currentUnit;
				units.add(cu);
				Unit nextUnit = updateStackFrame(cursf, currentTuple);
				if (nextUnit == null)
					stackTrace.pop();
			}
		}
		return units;
	}

	public void startWalk() {

		handleClInit(tds);
		// for each thread
		for (String s : tds.outputs.keySet()) {
			if (!s.equals("0")) {
				threadEvent = 0;
				currentPo = "O_" + s + "_0";
			}
			System.out.println("Walking " + s);
			// gets it entry point tuple
			List<AbsUnit> trace = generateIntraTraceForThread(s);
			System.out.println("==================TraceBegin=======================");
			for (AbsUnit u : trace) {
				if (!(u.u.toString().contains("java.lang.StringBuilder") || (u.u.toString().contains("PoP_."))
						|| (u.u.toString().contains("popUtil."))))
					System.out.println(u.u.toString());
			}
			System.out.println("==================TraceEnd=======================");
			// start looping
			for (AbsUnit cu : trace) {
				handleUnit(cu.u, cu.t, cu.sf);
			}

		}

	}

	boolean checkSkipList(Body b) {
		for (String s : skipList) {
			if (s.equals(b.getMethod().getDeclaringClass().getName()))
				return true;
		}
		return false;
	}

	void printConstrains() {
		System.out.println("Printing Constraints " + Integer.toString(constrains.size()));
		System.out.println("ProgramOrderConstrains");
		for (String s : poConstraints) {
			System.out.println(s);
		}

		System.out.println("RW Constrains");

		for (ReadWriteTuple t : readWriteTuples) {
			System.out.println(t.po + " " + t.TID + "," + t.read + "," + t.variableName + t.symBolicValue);

		}
		System.out.println("LU Constrains");

		for (LockUnlockTuple t : lockUnlockTuples) {

			System.out.println(t.po + " " + t.TID + "," + t.lock + "," + t.lockObject);
		}
		System.out.println("FJ Constrains");
		for (ForkJoinTuple t : forkJoinTuples) {
			System.out.println(t.po + " " + t.TID + "," + t.fork + "," + t.childTID);
		}

		System.out.println("Path Constrains");
		for (String t : pathConstraints) {
			System.out.println(t);
		}

		System.out.println("Assign Constrains");
		for (String t : assignConstrains) {
			System.out.println(t);
		}

	}

	void printRWTuples() {
		try {
			BufferedWriter bw = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream("sootOutput/RW_Tuples", false)));
			for (ReadWriteTuple t : readWriteTuples) {
				String toWrite = "";
				toWrite = toWrite + t.po + "|" + t.TID + "|" + t.variableName + "|" + t.symBolicValue + "\n";
				bw.write(toWrite);
			}
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	void printLUTuples() {
		try {
			BufferedWriter bw = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream("sootOutput/LU_Tuples", false)));
			for (LockUnlockTuple t : lockUnlockTuples) {
				String toWrite = "";
				toWrite = toWrite + t.po + "|" + t.TID + "|" + t.lock + "|" + t.lockObject + "\n";
				bw.write(toWrite);
			}
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	void printFJTuples() {
		try {
			BufferedWriter bw = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream("sootOutput/FJ_Tuples", false)));
			for (ForkJoinTuple t : forkJoinTuples) {
				String toWrite = "";
				toWrite = toWrite + t.po + "|" + t.TID + "|" + t.fork + "|" + t.childTID + "\n";
				bw.write(toWrite);
			}
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	void collectPathConstraints(Context ctx, Solver sol) {
		int i = 0;
		for (String s : pathConstraints) {
			BoolExpr be = null;
			String split[] = s.split(" ");
			if (split.length == 4) {
				if (!isNumeric(split[1])) {
					IntExpr iel = ctx.mkIntConst(split[1]);
					if (!isNumeric(split[3])) {
						IntExpr ier = ctx.mkIntConst(split[3]);
						if (split[0].equals("Yes")) {
							if (split[2].equals("!=")) {
								be = ctx.mkEq(iel, ier);
								be = ctx.mkNot(be);
							} else if (split[2].equals("==")) {
								be = ctx.mkEq(iel, ier);
							} else if (split[2].equals("<=")) {
								be = ctx.mkLe(iel, ier);
							} else if (split[2].equals(">=")) {
								be = ctx.mkGe(iel, ier);
							} else if (split[2].equals("<")) {
								be = ctx.mkLt(iel, ier);
							} else if (split[2].equals(">")) {
								be = ctx.mkGt(iel, ier);
							}
						} else {

							if (split[2].equals("!=")) {
								be = ctx.mkEq(iel, ier);
							} else if (split[2].equals("==")) {
								be = ctx.mkEq(iel, ier);
								be = ctx.mkNot(be);
							} else if (split[2].equals("<=")) {
								be = ctx.mkGt(iel, ier);
							} else if (split[2].equals(">=")) {
								be = ctx.mkLt(iel, ier);
							} else if (split[2].equals("<")) {
								be = ctx.mkGe(iel, ier);
							} else if (split[2].equals(">")) {
								be = ctx.mkLe(iel, ier);
							}

						}
					} else {
						IntNum r = ctx.mkInt(Integer.parseInt(split[3].trim()));
						if (split[0].equals("Yes")) {
							if (split[2].equals("!=")) {
								be = ctx.mkEq(iel, r);
								be = ctx.mkNot(be);
							} else if (split[2].equals("==")) {
								be = ctx.mkEq(iel, r);
							} else if (split[2].equals("<=")) {
								be = ctx.mkLe(iel, r);
							} else if (split[2].equals(">=")) {
								be = ctx.mkGe(iel, r);
							} else if (split[2].equals("<")) {
								be = ctx.mkLt(iel, r);
							} else if (split[2].equals(">")) {
								be = ctx.mkGt(iel, r);
							}
						} else {

							if (split[2].equals("!=")) {
								be = ctx.mkEq(iel, r);
							} else if (split[2].equals("==")) {
								be = ctx.mkEq(iel, r);
								be = ctx.mkNot(be);
							} else if (split[2].equals("<=")) {
								be = ctx.mkGt(iel, r);
							} else if (split[2].equals(">=")) {
								be = ctx.mkLt(iel, r);
							} else if (split[2].equals("<")) {
								be = ctx.mkGe(iel, r);
							} else if (split[2].equals(">")) {
								be = ctx.mkLe(iel, r);
							}

						}
					}

				} else {
					IntNum l = ctx.mkInt(Integer.parseInt(split[1].trim()));
					if (!isNumeric(split[3])) {
						IntExpr ier = ctx.mkIntConst(split[1]);
						if (split[0].equals("Yes")) {
							if (split[2].equals("!=")) {
								be = ctx.mkEq(l, ier);
								be = ctx.mkNot(be);
							} else if (split[2].equals("==")) {
								be = ctx.mkEq(l, ier);
							} else if (split[2].equals("<=")) {
								be = ctx.mkLe(l, ier);
							} else if (split[2].equals(">=")) {
								be = ctx.mkGe(l, ier);
							} else if (split[2].equals("<")) {
								be = ctx.mkLt(l, ier);
							} else if (split[2].equals(">")) {
								be = ctx.mkGt(l, ier);
							}
						} else {

							if (split[2].equals("!=")) {
								be = ctx.mkEq(l, ier);
							} else if (split[2].equals("==")) {
								be = ctx.mkEq(l, ier);
								be = ctx.mkNot(be);
							} else if (split[2].equals("<=")) {
								be = ctx.mkGt(l, ier);
							} else if (split[2].equals(">=")) {
								be = ctx.mkLt(l, ier);
							} else if (split[2].equals("<")) {
								be = ctx.mkGe(l, ier);
							} else if (split[2].equals(">")) {
								be = ctx.mkLe(l, ier);
							}

						}
					} else {
						IntNum r = ctx.mkInt(split[3]);
						if (split[0].equals("Yes")) {
							if (split[2].equals("!=")) {
								be = ctx.mkEq(l, r);
								be = ctx.mkNot(be);
							} else if (split[2].equals("==")) {
								be = ctx.mkEq(l, r);
							} else if (split[2].equals("<=")) {
								be = ctx.mkLe(l, r);
							} else if (split[2].equals(">=")) {
								be = ctx.mkGe(l, r);
							} else if (split[2].equals("<")) {
								be = ctx.mkLt(l, r);
							} else if (split[2].equals(">")) {
								be = ctx.mkGt(l, r);
							}
						} else {

							if (split[2].equals("!=")) {
								be = ctx.mkEq(l, r);
							} else if (split[2].equals("==")) {
								be = ctx.mkEq(l, r);
								be = ctx.mkNot(be);
							} else if (split[2].equals("<=")) {
								be = ctx.mkGt(l, r);
							} else if (split[2].equals(">=")) {
								be = ctx.mkLt(l, r);
							} else if (split[0].equals("Yes")) {

							} else {

							}
							if (split[2].equals("<")) {
								be = ctx.mkGe(l, r);
							} else if (split[2].equals(">")) {
								be = ctx.mkLe(l, r);
							}

						}
					}

				}
			} else {
				if (split[0].equals("Yes")) {
					if (isNumeric(split[1])) {

					} else {
						IntExpr ie = ctx.mkIntConst(split[1]);
						be = ctx.mkEq(ie, ctx.mkInt(1));
					}
				} else {
					if (isNumeric(split[1])) {

					} else {
						IntExpr ie = ctx.mkIntConst(split[1]);
						be = ctx.mkEq(ie, ctx.mkInt(1));
						be = ctx.mkNot(be);
					}
				}
			}
			String track = "path" + Integer.toString(i);
			sol.assertAndTrack(be, ctx.mkBoolConst(track));
			i++;
		}

	}

	public boolean isNumeric(String str) {
		try {
			double d = Double.parseDouble(str);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

	void collectPOConstrains(Context ctx, Solver s) {
		int i = 0;
		for (String p : poConstraints) {
			String c[] = p.split("<");
			IntExpr l = ctx.mkIntConst(c[0].trim());
			IntExpr r = ctx.mkIntConst(c[1].trim());
			BoolExpr b = ctx.mkLt(l, r);
			String track = "po" + Integer.toString(i);
			s.assertAndTrack(b, ctx.mkBoolConst(track));
			i++;
		}
	}

	void collectRWConstrains(Context ctx, Solver s) {
		int ic = 0;
		for (ReadWriteTuple i : readWriteTuples) {
			if (i.read == false)
				continue;
			BoolExpr outer = ctx.mkBool(false);
			for (ReadWriteTuple j : readWriteTuples) {
				if (j.read == true) {
					continue;
				}
				if (i.variableName.equals(j.variableName)) {
					IntExpr read = ctx.mkIntConst(i.symBolicValue);
					IntExpr w = ctx.mkIntConst(j.symBolicValue);
					BoolExpr eq = ctx.mkEq(read, w);

					for (ReadWriteTuple k : readWriteTuples) {
						if (i == k || k == j)
							continue;
						if (k.read == true)
							continue;
						if (!k.variableName.equals(i.variableName))
							continue;
						IntExpr kpo = ctx.mkIntConst(k.po);
						IntExpr ipo = ctx.mkIntConst(i.po);
						IntExpr jpo = ctx.mkIntConst(j.po);
						BoolExpr wbr = ctx.mkLt(jpo, ipo);
						BoolExpr lt = ctx.mkLt(kpo, jpo);
						BoolExpr gt = ctx.mkGt(kpo, ipo);
						BoolExpr or = ctx.mkOr(lt, gt);
						eq = ctx.mkAnd(eq, or);
						eq = ctx.mkAnd(eq, wbr);
					}
					outer = ctx.mkOr(outer, eq);
				}

			}
			// s.add(outer);
			String track = "rw" + Integer.toString(ic);
			s.assertAndTrack(outer, ctx.mkBoolConst(track));
			ic++;
		}
		System.out.println(ic);
	}

	LockUnlockTuple nextUnlock(LockUnlockTuple t, int i) {
		for (i = i + 1; i < lockUnlockTuples.size(); i++) {
			LockUnlockTuple ti = lockUnlockTuples.get(i);
			if (ti.lockObject.equals(t.lockObject) && ti.lock == false) {
				return ti;
			}

		}
		return null;
	}

	void collectLUConstraints(Context ctx, Solver s) {

		int i, j = 0;
		BoolExpr outer = ctx.mkBool(true);
		for (i = 0; i < lockUnlockTuples.size(); i++) {
			LockUnlockTuple ti, tj;
			ti = lockUnlockTuples.get(i);
			if (!ti.lock)
				continue;
			System.out.println("i:" + i);
			BoolExpr union = ctx.mkBool(false);
			for (j = 0; j < lockUnlockTuples.size(); j++) {
				tj = lockUnlockTuples.get(j);
				if (!tj.lock)
					continue;
				if (!ti.lockObject.equals(tj.lockObject))
					continue;
				if (ti.TID.equals(tj.TID))
					continue;
				System.out.println("j:" + j);
				IntExpr rejLock = ctx.mkIntConst(tj.po);
				LockUnlockTuple t = nextUnlock(ti, i);
				IntExpr holdingLock = ctx.mkIntConst(ti.po);
				IntExpr holdingUnlock = ctx.mkIntConst(t.po);
				BoolExpr lt = ctx.mkLt(rejLock, holdingLock);
				BoolExpr gt = ctx.mkGt(rejLock, holdingUnlock);
				union = ctx.mkOr(lt, gt);
				outer = ctx.mkAnd(outer, union);
			}
		}
		String track = "lu" + Integer.toString(0);
		s.assertAndTrack(outer, ctx.mkBoolConst(track));
		// s.add(outer);
	}

	void CollectForkJoinConstraints(Context ctx, Solver s) {
		// forks
		int ic = 0;
		for (ForkJoinTuple t : forkJoinTuples) {
			if (t.fork) {
				String childPoStr = "O_" + t.childTID + "_0";
				IntExpr parentPo = ctx.mkIntConst(t.po);
				IntExpr childPo = ctx.mkIntConst(childPoStr);
				BoolExpr gt = ctx.mkGt(childPo, parentPo);
				// s.add(gt);
				String track = "f" + Integer.toString(ic);
				s.assertAndTrack(gt, ctx.mkBoolConst(track));
			} else {
				String posplit[] = t.po.split("_");
				// String parentNextPostr = posplit[0].trim() + "_" + posplit[1]
				// + "_"
				// + Integer.toString((new Integer(posplit[2]).intValue()) + 1);

				String childpostr = lastProgramOrder.get(t.childTID);
				IntExpr ppo = ctx.mkIntConst(t.po);
				IntExpr cpo = ctx.mkIntConst(childpostr);
				BoolExpr lt = ctx.mkLt(cpo, ppo);
				// s.add(lt);
				String track = "j" + Integer.toString(ic);
				s.assertAndTrack(lt, ctx.mkBoolConst(track));
			}
			ic++;
		}

		// Joins

	}

	void collectAssignConstraints(Context ctx, Solver s) {
		int i = 0;
		for (String ac : assignConstrains) {
			System.out.println("AC " + ac);
			if (ac.contains("+")) {
				String split[] = ac.split("=");
				IntExpr iel = ctx.mkIntConst(split[0].trim());
				String subSplit[] = split[1].split("\\+");
				if (!isNumeric(subSplit[0]) && !isNumeric(subSplit[1])) {
					IntExpr r1 = ctx.mkIntConst(subSplit[0].trim());
					IntExpr r2 = ctx.mkIntConst(subSplit[1].trim());
					ArithExpr ier = ctx.mkAdd(r1, r2);
					BoolExpr be = ctx.mkEq(iel, ier);

					String track = "as" + Integer.toString(i);
					s.assertAndTrack(be, ctx.mkBoolConst(track));

				} else if (isNumeric(subSplit[0]) && !isNumeric(subSplit[1])) {
					IntNum r1 = ctx.mkInt(Integer.parseInt(subSplit[0].trim()));
					IntExpr r2 = ctx.mkIntConst(subSplit[1].trim());
					ArithExpr ier = ctx.mkAdd(r1, r2);
					BoolExpr be = ctx.mkEq(iel, ier);
					String track = "as" + Integer.toString(i);
					s.assertAndTrack(be, ctx.mkBoolConst(track));
					System.out.println("(&*(&" + be);
				} else if (!isNumeric(subSplit[0]) && isNumeric(subSplit[1])) {
					IntExpr r1 = ctx.mkIntConst(subSplit[0].trim());
					IntNum r2 = ctx.mkInt(Integer.parseInt(subSplit[1].trim()));
					ArithExpr ier = ctx.mkAdd(r1, r2);
					BoolExpr be = ctx.mkEq(iel, ier);
					String track = "as" + Integer.toString(i);
					s.assertAndTrack(be, ctx.mkBoolConst(track));
				} else if (isNumeric(subSplit[0]) && isNumeric(subSplit[1])) {
					IntNum r1 = ctx.mkInt(Integer.parseInt(subSplit[0].trim()));
					IntNum r2 = ctx.mkInt(Integer.parseInt(subSplit[1].trim()));
					ArithExpr ier = ctx.mkAdd(r1, r2);
					BoolExpr be = ctx.mkEq(iel, ier);
					String track = "as" + Integer.toString(i);
					s.assertAndTrack(be, ctx.mkBoolConst(track));
				}

			} else if (ac.contains("-")) {
				String split[] = ac.split("=");
				IntExpr iel = ctx.mkIntConst(split[0].trim());
				String subSplit[] = split[1].split("-");
				if (!isNumeric(subSplit[0]) && !isNumeric(subSplit[1])) {
					IntExpr r1 = ctx.mkIntConst(subSplit[0].trim());
					IntExpr r2 = ctx.mkIntConst(subSplit[1].trim());
					ArithExpr ier = ctx.mkSub(r1, r2);
					BoolExpr be = ctx.mkEq(iel, ier);
					String track = "as" + Integer.toString(i);
					s.assertAndTrack(be, ctx.mkBoolConst(track));
				} else if (isNumeric(subSplit[0]) && !isNumeric(subSplit[1])) {
					IntNum r1 = ctx.mkInt(new Integer(subSplit[0].trim()));
					IntExpr r2 = ctx.mkIntConst(subSplit[1].trim());
					ArithExpr ier = ctx.mkSub(r1, r2);
					BoolExpr be = ctx.mkEq(iel, ier);
					String track = "as" + Integer.toString(i);
					s.assertAndTrack(be, ctx.mkBoolConst(track));
				} else if (!isNumeric(subSplit[0]) && isNumeric(subSplit[1])) {
					IntExpr r1 = ctx.mkIntConst(subSplit[0].trim());
					IntNum r2 = ctx.mkInt(new Integer(subSplit[1].trim()));
					ArithExpr ier = ctx.mkSub(r1, r2);
					BoolExpr be = ctx.mkEq(iel, ier);
					String track = "as" + Integer.toString(i);
					s.assertAndTrack(be, ctx.mkBoolConst(track));
				} else if (isNumeric(subSplit[0]) && isNumeric(subSplit[1])) {
					IntNum r1 = ctx.mkInt(new Integer(subSplit[0].trim()));
					IntNum r2 = ctx.mkInt(new Integer(subSplit[1].trim()));
					ArithExpr ier = ctx.mkSub(r1, r2);
					BoolExpr be = ctx.mkEq(iel, ier);
					String track = "as" + Integer.toString(i);
					s.assertAndTrack(be, ctx.mkBoolConst(track));
				}
			} else if (ac.contains("*")) {
				String split[] = ac.split("=");
				IntExpr iel = ctx.mkIntConst(split[0].trim());
				String subSplit[] = split[1].split("\\*");
				if (!isNumeric(subSplit[0]) && !isNumeric(subSplit[1])) {
					IntExpr r1 = ctx.mkIntConst(subSplit[0].trim());
					IntExpr r2 = ctx.mkIntConst(subSplit[1].trim());
					ArithExpr ier = ctx.mkMul(r1, r2);
					BoolExpr be = ctx.mkEq(iel, ier);
					String track = "as" + Integer.toString(i);
					s.assertAndTrack(be, ctx.mkBoolConst(track));
				} else if (isNumeric(subSplit[0]) && !isNumeric(subSplit[1])) {
					IntNum r1 = ctx.mkInt(new Integer(subSplit[0].trim()));
					IntExpr r2 = ctx.mkIntConst(subSplit[1].trim());
					ArithExpr ier = ctx.mkMul(r1, r2);
					BoolExpr be = ctx.mkEq(iel, ier);
					String track = "as" + Integer.toString(i);
					s.assertAndTrack(be, ctx.mkBoolConst(track));
				} else if (!isNumeric(subSplit[0]) && isNumeric(subSplit[1])) {
					IntExpr r1 = ctx.mkIntConst(subSplit[0].trim());
					IntNum r2 = ctx.mkInt(new Integer(subSplit[1].trim()));
					ArithExpr ier = ctx.mkMul(r1, r2);
					BoolExpr be = ctx.mkEq(iel, ier);
					String track = "as" + Integer.toString(i);
					s.assertAndTrack(be, ctx.mkBoolConst(track));
				} else if (isNumeric(subSplit[0]) && isNumeric(subSplit[1])) {
					IntNum r1 = ctx.mkInt(new Integer(subSplit[0].trim()));
					IntNum r2 = ctx.mkInt(new Integer(subSplit[1].trim()));
					ArithExpr ier = ctx.mkMul(r1, r2);
					BoolExpr be = ctx.mkEq(iel, ier);
					String track = "as" + Integer.toString(i);
					s.assertAndTrack(be, ctx.mkBoolConst(track));
				}
			} else if (ac.contains("/")) {
				String split[] = ac.split("=");
				IntExpr iel = ctx.mkIntConst(split[0].trim());
				String subSplit[] = split[1].split("/");
				if (!isNumeric(subSplit[0]) && !isNumeric(subSplit[1])) {
					IntExpr r1 = ctx.mkIntConst(subSplit[0].trim());
					IntExpr r2 = ctx.mkIntConst(subSplit[1].trim());
					ArithExpr ier = ctx.mkDiv(r1, r2);
					BoolExpr be = ctx.mkEq(iel, ier);
					String track = "as" + Integer.toString(i);
					s.assertAndTrack(be, ctx.mkBoolConst(track));
				} else if (isNumeric(subSplit[0]) && !isNumeric(subSplit[1])) {
					IntNum r1 = ctx.mkInt(new Integer(subSplit[0].trim()));
					IntExpr r2 = ctx.mkIntConst(subSplit[1].trim());
					ArithExpr ier = ctx.mkDiv(r1, r2);
					BoolExpr be = ctx.mkEq(iel, ier);
					String track = "as" + Integer.toString(i);
					s.assertAndTrack(be, ctx.mkBoolConst(track));
				} else if (!isNumeric(subSplit[0]) && isNumeric(subSplit[1])) {
					IntExpr r1 = ctx.mkIntConst(subSplit[0].trim());
					IntNum r2 = ctx.mkInt(new Integer(subSplit[1].trim()));
					ArithExpr ier = ctx.mkDiv(r1, r2);
					BoolExpr be = ctx.mkEq(iel, ier);
					String track = "as" + Integer.toString(i);
					s.assertAndTrack(be, ctx.mkBoolConst(track));
				} else if (isNumeric(subSplit[0]) && isNumeric(subSplit[1])) {
					IntNum r1 = ctx.mkInt(new Integer(subSplit[0].trim()));
					IntNum r2 = ctx.mkInt(new Integer(subSplit[1].trim()));
					ArithExpr ier = ctx.mkDiv(r1, r2);
					BoolExpr be = ctx.mkEq(iel, ier);
					String track = "as" + Integer.toString(i);
					s.assertAndTrack(be, ctx.mkBoolConst(track));
				}
			} else if (ac.contains("%")) {
				String split[] = ac.split("=");
				IntExpr iel = ctx.mkIntConst(split[0].trim());
				String subSplit[] = split[1].split("%");
				if (!isNumeric(subSplit[0]) && !isNumeric(subSplit[1])) {
					IntExpr r1 = ctx.mkIntConst(subSplit[0].trim());
					IntExpr r2 = ctx.mkIntConst(subSplit[1].trim());
					ArithExpr ier = ctx.mkMod(r1, r2);
					BoolExpr be = ctx.mkEq(iel, ier);
					String track = "as" + Integer.toString(i);
					s.assertAndTrack(be, ctx.mkBoolConst(track));
				} else if (isNumeric(subSplit[0]) && !isNumeric(subSplit[1])) {
					IntNum r1 = ctx.mkInt(new Integer(subSplit[0].trim()));
					IntExpr r2 = ctx.mkIntConst(subSplit[1].trim());
					ArithExpr ier = ctx.mkMod(r1, r2);
					BoolExpr be = ctx.mkEq(iel, ier);
					String track = "as" + Integer.toString(i);
					s.assertAndTrack(be, ctx.mkBoolConst(track));
				} else if (!isNumeric(subSplit[0]) && isNumeric(subSplit[1])) {
					IntExpr r1 = ctx.mkIntConst(subSplit[0].trim());
					IntNum r2 = ctx.mkInt(new Integer(subSplit[1].trim()));
					ArithExpr ier = ctx.mkMod(r1, r2);
					BoolExpr be = ctx.mkEq(iel, ier);
					String track = "as" + Integer.toString(i);
					s.assertAndTrack(be, ctx.mkBoolConst(track));
				} else if (isNumeric(subSplit[0]) && isNumeric(subSplit[1])) {
					IntNum r1 = ctx.mkInt(new Integer(subSplit[0].trim()));
					IntNum r2 = ctx.mkInt(new Integer(subSplit[1].trim()));
					ArithExpr ier = ctx.mkMod(r1, r2);
					BoolExpr be = ctx.mkEq(iel, ier);
					String track = "as" + Integer.toString(i);
					s.assertAndTrack(be, ctx.mkBoolConst(track));
				}
			} else if (ac.contains("^")) {

				String split[] = ac.split("=");
				IntExpr iel = ctx.mkIntConst(split[0].trim());
				String subSplit[] = split[1].split("\\^");
				if (!isNumeric(subSplit[0]) && !isNumeric(subSplit[1])) {
					BoolExpr r1 = ctx.mkBoolConst(subSplit[0].trim());
					BoolExpr r2 = ctx.mkBoolConst(subSplit[1].trim());
					BoolExpr ier = ctx.mkXor(r1, r2);
					BoolExpr be = ctx.mkEq(iel, ier);
					String track = "as" + Integer.toString(i);
					s.assertAndTrack(be, ctx.mkBoolConst(track));
				} else if (isNumeric(subSplit[0]) && !isNumeric(subSplit[1])) {
					BoolExpr r1 = ctx.mkBool(subSplit[0].trim().equals("1"));
					BoolExpr r2 = ctx.mkBoolConst(subSplit[1].trim());
					BoolExpr ier = ctx.mkXor(r1, r2);
					BoolExpr be = ctx.mkEq(iel, ier);
					String track = "as" + Integer.toString(i);
					s.assertAndTrack(be, ctx.mkBoolConst(track));
				} else if (!isNumeric(subSplit[0]) && isNumeric(subSplit[1])) {
					BitVecExpr r1 = ctx.mkInt2BV(32, ctx.mkIntConst(subSplit[0].trim()));
					BitVecExpr r2 = ctx.mkBV(Integer.parseInt(subSplit[1].trim()), 32);
					BitVecExpr ier = ctx.mkBVXOR(r1, r2);
					Expr e = ctx.mkBV2Int(ier, true);
					BoolExpr be = ctx.mkEq(iel, e);

					String track = "as" + Integer.toString(i);
					s.assertAndTrack(be, ctx.mkBoolConst(track));
				} else if (isNumeric(subSplit[0]) && isNumeric(subSplit[1])) {
					BoolExpr r1 = ctx.mkBool(subSplit[0].trim().equals("1"));
					BoolExpr r2 = ctx.mkBool(subSplit[1].trim().equals("1"));
					BoolExpr ier = ctx.mkXor(r1, r2);
					BoolExpr be = ctx.mkEq(iel, ier);
					String track = "as" + Integer.toString(i);
					s.assertAndTrack(be, ctx.mkBoolConst(track));
				}
			} else {
				String split[] = ac.split("=");
				if (isNumeric(split[1].trim())) {
					// r is numeric
					IntNum in = ctx.mkInt(new Integer(split[1].trim()).intValue());
					IntExpr ie = ctx.mkIntConst(split[0].trim());
					BoolExpr be = ctx.mkEq(in, ie);
					String track = "as" + Integer.toString(i);
					s.assertAndTrack(be, ctx.mkBoolConst(track));
				} else {
					IntExpr iel = ctx.mkIntConst(split[0].trim());
					IntExpr ier = ctx.mkIntConst(split[1].trim());
					BoolExpr be = ctx.mkEq(iel, ier);
					String track = "as" + Integer.toString(i);
					s.assertAndTrack(be, ctx.mkBoolConst(track));
				}
			}
			i++;
		}
	}

	List<String> sortEvaluatedPo() {
		List<String> l = new ArrayList<String>();
		String remove = null;
		HashMap<String, Integer> copy = new HashMap<String, Integer>(evaluatedProgramOrders);
		while (copy.size() > 0) {
			int min = 100000;
			for (String s : copy.keySet()) {
				if (copy.get(s) <= min) {
					min = copy.get(s).intValue();
					remove = s;
				}
			}
			l.add(remove);
			copy.remove(remove);
		}
		return l;
	}

	ReadWriteTuple getRWTupleFromPo(String po) {
		for (ReadWriteTuple rw : readWriteTuples)
			if (po.equals(rw.po))
				return rw;
		return null;
	}

	ForkJoinTuple getFJTupleFromPo(String po) {
		for (ForkJoinTuple rw : forkJoinTuples)
			if (po.equals(rw.po))
				return rw;
		return null;
	}

	LockUnlockTuple getLUTupleFromPo(String po) {
		for (LockUnlockTuple rw : lockUnlockTuples)
			if (po.equals(rw.po))
				return rw;
		return null;
	}

	BeginEndTuple getBETupleFromPo(String po) {
		for (BeginEndTuple rw : bETuples)
			if (po.equals(rw.po))
				return rw;
		return null;
	}

	void globalTraceWriter() {
		List<String> opt = new ArrayList<String>();
		List<String> s = sortEvaluatedPo();
		String line = "";
		line = "0, Begin";
		opt.add(line);
		System.out.println(line);
		for (String po : s) {
			line = "";
			if (getRWTupleFromPo(po) != null) {
				ReadWriteTuple t = getRWTupleFromPo(po);
				line = line + t.TID;
				line = line + ", ";
				if (t.read)
					line = line + "Read, ";
				else
					line = line + "Write, ";
				line = line + t.variableName + ", ";
				line = line + t.symBolicValue;
				if (!line.contains("java.util")) {
					opt.add(line);
					System.out.println(line);
				}
			} else if (getLUTupleFromPo(po) != null) {
				LockUnlockTuple t = getLUTupleFromPo(po);
				line = line + t.TID + ", ";
				if (t.lock)
					line = line + "Lock, ";
				else
					line = line + "Unlock, ";
				line = line + t.lockObject;
				opt.add(line);
				System.out.println(line);
			} else if (getFJTupleFromPo(po) != null) {
				ForkJoinTuple t = getFJTupleFromPo(po);
				line = line + t.TID + ", ";
				if (t.fork)
					line = line + "Fork, ";
				else
					line = line + "Join, ";
				line = line + t.childTID;
				opt.add(line);
				System.out.println(line);
			} else if (getBETupleFromPo(po) != null) {
				BeginEndTuple t = getBETupleFromPo(po);
				line = line + t.TID + ", ";
				if (t.begin)
					line = line + "Begin";
				else
					line = line + "End";
				opt.add(line);
				System.out.println(line);
			}
		}
		line = "0, End";
		opt.add(line);
		System.out.println(line);
		try {
			BufferedWriter bw = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream("sootOutput/globalTrace", false)));
			for (int i = 0; i < opt.size() - 1; i++) {
				bw.write(opt.get(i));
				bw.newLine();
			}
			bw.write(opt.get(opt.size() - 1));
			bw.close();

			bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("sootOutput/Tuples", false)));
			for (String st : tds.outputs.keySet()) {
				for (Tuple t : tds.outputs.get(st)) {
					String l = "<" + t.methodSignature + ", " + t.threadId + ", " + Long.toString(t.previouslyCalled)
							+ ", ";
					for (Integer i : t.path) {
						l = l + i.toString() + " ";
					}
					l = l + ">";
					bw.write(l);
					bw.newLine();
				}

			}
			bw.write(opt.get(opt.size() - 1));
			bw.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	void collectBeginEndConstrains(Context ctx, Solver s) {
		for (BeginEndTuple be : bETuples) {
			System.out.println(be.TID + "|" + be.po + "|" + be.correspondingPo + "|" + be.begin);
			if (be.begin) {
				String bepo = "O_" + be.TID + "_0";
				IntExpr cpo = ctx.mkIntConst(bepo);
				IntExpr tpo = ctx.mkIntConst(be.po);
				IntExpr parentpo = ctx.mkIntConst(be.correspondingPo);
				BoolExpr be1 = ctx.mkLt(tpo, cpo);
				BoolExpr be2 = ctx.mkLt(parentpo, tpo);
				s.add(be1);
				s.add(be2);
			} else {
				IntExpr ie = ctx.mkIntConst(be.po);
				IntExpr ce = ctx.mkIntConst(lastProgramOrder.get(be.TID));
				BoolExpr ae = ctx.mkGt(ie, ce);
				s.add(ae);

				lastProgramOrder.remove(be.TID);
				lastProgramOrder.put(be.TID, be.po);
			}
			programOrders.add(be.po);
			Statement st = new Statement();
			st.TID = be.TID;
			st.statement = "BE";
			statements.put(be.po, st);
		}
	}

	void Solving() {

		HashMap<String, String> parm = new HashMap<String, String>();
		parm.put("unsat_core", "true");
		parm.put("model", "true");
		Context ctx = new Context(parm);
		Solver solver = ctx.mkSolver();

		collectPOConstrains(ctx, solver);
		collectBeginEndConstrains(ctx, solver);
		collectRWConstrains(ctx, solver);
		CollectForkJoinConstraints(ctx, solver);
		collectLUConstraints(ctx, solver);
		collectPathConstraints(ctx, solver);
		collectAssignConstraints(ctx, solver);
		String first = "O_0_0";
		IntExpr z = ctx.mkIntConst(first);
		IntNum ze = ctx.mkInt(1);
		BoolExpr be = ctx.mkEq(z, ze);
		solver.add(be);
		System.out.println("Solving");
		if (solver.check() == Status.SATISFIABLE) {
			System.out.println("SAT");
			System.out.println(solver);
			Model m = solver.getModel();
			for (String s : programOrders) {
				IntExpr o = ctx.mkIntConst(s);
				evaluatedProgramOrders.put(s, Integer.parseInt(m.eval(o, true).toString()));
			}
		} else {
			System.out.println("UNSAT");
			System.out.println(solver.toString());
			for (BoolExpr c : solver.getUnsatCore()) {
				System.out.println("core: " + c.toString());
			}
		}
		List<String> s = sortEvaluatedPo();
		String prevThread = "";
		for (String po : s) {
			if (!statements.get(po).TID.equals(prevThread)) {
				System.out.println("");
			}
			System.out.println(evaluatedProgramOrders.get(po) + "\t:\t" + statements.get(po).TID + "\t|\t"
					+ statements.get(po).statement);

			prevThread = statements.get(po).TID;
		}

	}

	void test() {
		System.out.println("Testing");
		tds.populateTuples();
		for (String s : tds.outputs.keySet()) {
			List<AbsUnit> list = generateIntraTraceForThread(s);
			System.out.println("after gen");
			System.out.println("Thread = " + s);
			for (AbsUnit u : list) {
				System.out.println(u);
			}
		}
	}

	@Override
	protected void internalTransform(String phaseName, Map<String, String> options) {

		/*
		 * SceneTransformer vs BodyTransformer
		 * =================================== BodyTransformer is applied on all
		 * methods separately. It uses multiple worker threads to process the
		 * methods in parallel. SceneTransformer is applied on the whole program
		 * in one go. SceneTransformer uses only one worker thread.
		 * 
		 * It is better to use SceneTransformer for this part of the project
		 * because: 1. During symbolic execution you may need to 'jump' from one
		 * method to another when you encounter a call instruction. This is
		 * easily done in SceneTransformer (see below) 2. There is only one
		 * worker thread in SceneTransformer which saves you from having to
		 * synchronize accesses to any global data structures that you'll use,
		 * (eg data structures to store constraints, trace, etc)
		 * 
		 * How to get method body? ======================= Use
		 * Scene.v().getApplicationClasses() to get all classes and
		 * SootClass::getMethods() to get all methods in a particular class. You
		 * can search these lists to find the body of any method.
		 * 
		 */

		/*
		 * Perform the following for each thread T: 1. Start with the first
		 * method of thread T. If T is the main thread, the first method is
		 * main(), else it is the run() method of the thread's class 2. Using
		 * (only) the tuples for thread T, walk through the code to reproduce
		 * the path followed by T. If thread T performs method calls then,
		 * during this walk you'll need to jump from one method's body to
		 * another to imitate the flow of control (as discussed during the
		 * Project session) 3. While walking through the code, collect the
		 * intra-thread trace for T and the path constraints. Don't forget that
		 * you need to renumber each dynamic instance of a static variable (i.e.
		 * SSA)
		 */
		initialize();
		startWalk();
		printConstrains();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Solving();
		globalTraceWriter();
		return;
	}

}