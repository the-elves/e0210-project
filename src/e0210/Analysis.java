package e0210;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/*
 * @author Sridhar Gopinath		-		g.sridhar53@gmail.com
 * 
 * Course project,
 * Principles of Programming Course, Fall - 2016,
 * Computer Science and Automation (CSA),
 * Indian Institute of Science (IISc),
 * Bangalore
 */

import java.util.Map;

import org.jgrapht.ext.DOTExporter;
import org.jgrapht.ext.VertexNameProvider;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DirectedPseudograph;

import soot.Body;
import soot.BodyTransformer;
import soot.Local;
import soot.LongType;
import soot.Scene;
import soot.SootClass;
import soot.SootMethod;
import soot.Unit;
import soot.Value;
import soot.jimple.AssignStmt;
import soot.jimple.InvokeExpr;
import soot.jimple.InvokeStmt;
import soot.jimple.Jimple;
import soot.jimple.JimpleBody;
import soot.jimple.SpecialInvokeExpr;
import soot.jimple.Stmt;
import soot.jimple.VirtualInvokeExpr;
import soot.toolkits.graph.Block;
import soot.util.Chain;

public class Analysis extends BodyTransformer {
	enum function {FUNCTIONCALLED, ADDTHREAD, GETCURRENTHREAD};
	DirectedPseudograph<Block, DefaultEdge> graph = new 
			DirectedPseudograph<Block, DefaultEdge>(DefaultEdge.class);
	SootClass threadClass=null;
	SootClass parentClass;
	SootClass TDS;
	SootMethod getTId;
	SootMethod addThread;
	SootMethod functionCalled;
	SootMethod getCurrentThread;
	SootMethod printMethod;
	SootMethod printDump;
	List<String> skiplist;
	boolean initialized=false;
	
	
	void removeThreadFiles(){
		File dir = new File("ThreadOutputs");
		if(dir.mkdir())
			System.out.println("Successfull");
		dir = new File("ThreadOutputs");
		File list[] = dir.listFiles();
		System.out.println("EXception");
		for(File f : list){
			if(f!=null)
			f.delete();
		}
	}
	void initialize(Body b){
		removeThreadFiles();
		skiplist = new ArrayList<String>();
		skiplist.add("Tuple");
		skiplist.add("TupleDataStore");
		skiplist.add("ThreadData");
		skiplist.add("popUtil");
		skiplist.add("ProcessOutput");
		skiplist.add("jdk");

		threadClass = Scene.v().getSootClass("java.lang.Thread");
		
		TDS = Scene.v().loadClassAndSupport("e0210.TupleDataStore");
		
		List<SootMethod> list = TDS.getMethods();
		getTId = threadClass.getMethod("long getId()");
		addThread = TDS.getMethod("void addThread(long)");
		functionCalled = TDS.getMethod("int functionCalled(java.lang.String)");
		getCurrentThread = threadClass.getMethod("java.lang.Thread currentThread()");
		printMethod = TDS.getMethod("void print(long,java.lang.String,int,int)");
		printDump = TDS.getMethod("void printDump()");
		
	}
	boolean skipCheck(String className){
		for(String skip:skiplist){
			if(className.contains(skip)){
				//System.out.println(className + "Skipped");
				return true;
			}
		}
		return false;
	}
	
	boolean hasLocalWithName(Body b,String name){
		for(Local l:b.getLocals()){
			if(l.getName().equals(name))
				return true;
		}
		return false;
	}
	
	void addThreadCall(Stmt before, Body b,
			Chain<Unit> in, SpecialInvokeExpr expr){
		InvokeExpr ie;
		AssignStmt as;
		Local thid = null;
		if(!hasLocalWithName(b, "__thid__")){
			thid = Jimple.v().newLocal("__thid__", LongType.v());
			b.getLocals().add(thid);
		}
		else{
			for(Local l:b.getLocals()){
				if(l.getName().equals("__thid__"));
					thid = l;
			}
		}
		Local ts = (Local) expr.getBase();
		
		ie = Jimple.v().newVirtualInvokeExpr(ts,
				getTId.makeRef());
		as = Jimple.v().newAssignStmt(thid, ie);
		in.insertBefore(before, as);
		
		ie = Jimple.v().newStaticInvokeExpr(addThread.makeRef(),thid);
		
		InvokeStmt is = Jimple.v().newInvokeStmt(ie);
		in.insertBefore(is, before);
	}
	
	void addThreadCall(Stmt before, Body b,
			Chain<Unit> in, Local th){
		InvokeExpr ie;
		AssignStmt as;
		
		Local thid = Jimple.v().newLocal("__thid__", LongType.v());
		
		b.getLocals().add(thid);
		
		
		ie = Jimple.v().newVirtualInvokeExpr(th,
				getTId.makeRef());
		as = Jimple.v().newAssignStmt(thid, ie);
		in.insertBefore(as, before);
		
		ie = Jimple.v().newStaticInvokeExpr(addThread.makeRef(),thid);
		InvokeStmt is = Jimple.v().newInvokeStmt(ie);
		in.insertBefore(is, before);
	}
	
	
	void iterateBody(Body b){

		Iterator body = b.getUnits().snapshotIterator();
		while(body.hasNext()){
			Stmt s = (Stmt)(body.next());
			if(s instanceof InvokeStmt){
				InvokeExpr ie = ((InvokeStmt)s).getInvokeExpr();
				String method = ie.getMethod().getName();
				if(method.equals("start")){
					Local l = ((Local)((VirtualInvokeExpr)ie).getBase());
					addThreadCall(s, b, b.getUnits(), l);
				}
			}
				
		}	
	}
	
	@Override
	protected synchronized void internalTransform(Body b, String phaseName, Map<String, String> options) {
		JimpleBody jb=(JimpleBody)b;
		
		if(!initialized){
			initialize(b);   
			initialized=true;
		}
		
		if(skipCheck(b.getMethod().getDeclaringClass().getName()))
			return;
		
		if(b.getMethod().getName().equals("main") || b.getMethod().getName().equals("<clinit>")){
			SootClass c =b.getMethod().getDeclaringClass();
			Stmt firstNonIdentity = jb.getFirstNonIdentityStmt();
			Local mt = Jimple.v().newLocal("__mainthread__", threadClass.getType());
			InvokeExpr ie = Jimple.v().newStaticInvokeExpr(getCurrentThread.makeRef());
			b.getLocals().add(mt);
			AssignStmt as = Jimple.v().newAssignStmt(mt, ie);
			b.getUnits().insertBefore(as,firstNonIdentity);
			addThreadCall(firstNonIdentity, b, b.getUnits(), mt);		
		}
		iterateBody(b);
		
		BLInstrumentation bl = new BLInstrumentation(printMethod,functionCalled,printDump);
		bl.internalTransform(b);
		
//		System.out.println(b.toString());
		
		return;
		
	}

	public void finish(String testcase) {

		VertexNameProvider<Block> id = new VertexNameProvider<Block>() {

			@Override
			public String getVertexName(Block b) {
				return String.valueOf("\"" + b.getBody().getMethod().getNumber() + " " + b.getIndexInMethod() + "\"");
			}
		};

		VertexNameProvider<Block> name = new VertexNameProvider<Block>() {

			@Override
			public String getVertexName(Block b) {
				String body = b.toString().replace("\'", "").replace("\"", "");
				return body;
			}
		};

		new File("sootOutput").mkdir();

		DOTExporter<Block, DefaultEdge> exporter = new DOTExporter<Block, DefaultEdge>(id, name, null);
		try {
			exporter.export(new PrintWriter("sootOutput/" + testcase + ".dot"), graph);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return;
	}

}