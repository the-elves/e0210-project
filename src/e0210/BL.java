package e0210;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

/*
 * @author Sridhar Gopinath		-		g.sridhar53@gmail.com

 * 
 * Course project,
 * Principles of Programming Course, Fall - 2016,
 * Computer Science and Automation (CSA),
 * Indian Institute of Science (IISc),
 * Bangalore
 */

/**
 *TODO think about switch
 *TODO think loops
 */
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;
import org.jgrapht.traverse.TopologicalOrderIterator;

import org.jgrapht.ext.DOTExporter;
import org.jgrapht.ext.VertexNameProvider;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DirectedPseudograph;

import soot.Body;
import soot.BodyTransformer;
import soot.Local;
import soot.RefType;
import soot.Scene;
import soot.SootClass;
import soot.SootField;
import soot.SootMethod;
import soot.Unit;
import soot.jimple.AddExpr;
import soot.jimple.AssignStmt;
import soot.jimple.InvokeExpr;
import soot.jimple.InvokeStmt;
import soot.jimple.Jimple;
import soot.jimple.JimpleBody;
import soot.jimple.LongConstant;
import soot.toolkits.graph.Block;
import soot.toolkits.graph.ExceptionalBlockGraph;
import soot.toolkits.graph.Block;
import soot.toolkits.graph.ExceptionalBlockGraph;

public class BL {
	boolean classVariableCreated=false;
	SootClass stream;
	SootMethod sopln,sopln_str;
	Local streamInstance;
	SootField classCounter;
	AssignStmt as;
	
	//implement class for loop info
	
	class BlockEdge extends DefaultEdge{
		int weight;
		public BlockEdge(int weight){
			setWeight(weight);
		}
		public int getWeight() {
			return weight;
		}
		public void setWeight(int weight) {
			this.weight = weight;
		}
		public String toString(){
			BlockVertex src=(BlockVertex)getSource();
			BlockVertex dest=(BlockVertex)getTarget();
			return ("(" + Integer.toString(src.getId()) +","+Integer.toString(dest.getId())+ ","+Integer.toString(weight) + ")");
		}
	}
	
	class BlockVertex{
		int id, label;
		public BlockVertex(int id, int label){
			setId(id);
			setLabel(label);
		}
		public int getLabel() {
			return label;
		}
		public void setLabel(int label) {
			this.label = label;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		
		public String toString(){
			return ("(" + Integer.toString(id) + ","+Integer.toString(label) + ")");
		}
	}
	
	public Block getBlockWithId(int id, ExceptionalBlockGraph cfg){
		boolean blockFound=false;
		Block currentBlock=null;
		for(Block bl:cfg.getBlocks())
			if(bl.getIndexInMethod() == id){
				blockFound = true;
				currentBlock = bl;
				break;
		}
		if (!blockFound)
			try {
				throw (new Exception());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return currentBlock;
		
	}
	
	
	public BlockVertex getBlockVertexWithId(int id, SimpleDirectedWeightedGraph<BlockVertex, BlockEdge> cfg){
		boolean blockFound=false;
		BlockVertex currentBlock=null;
		for(BlockVertex bl:cfg.vertexSet())
			if(bl.getId() == id){
				blockFound = true;
				currentBlock = bl;
				break;
		}
		if (!blockFound)
			try {
				throw (new Exception());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return currentBlock;
		
	}
	
	
	protected SimpleDirectedWeightedGraph<BlockVertex,BlockEdge> convertSootToJGraphT(ExceptionalBlockGraph cfg){
		
		SimpleDirectedWeightedGraph<BlockVertex,BlockEdge> convertedGraph = new SimpleDirectedWeightedGraph<BlockVertex,BlockEdge>(BlockEdge.class);
		int id = 0;
		for(Block bl : cfg.getBlocks()){
			convertedGraph.addVertex( new BlockVertex(bl.getIndexInMethod(), 0) );
		}
		
		Queue<Integer> currentNodes = new LinkedList<Integer>();
		Queue<Integer> visitedNodes = new LinkedList<Integer>();
		Queue<Integer> loopSourceNodes = new LinkedList<Integer>();
		
		for(Block bl: cfg.getHeads()){
			currentNodes.add(bl.getIndexInMethod());
		}
		
		while(!currentNodes.isEmpty()){
			Integer currentNode;
			
			currentNode = currentNodes.remove();
			//find block with block id currentNode
			Block currentBlock = getBlockWithId(currentNode, cfg);
			for(Block succ: currentBlock.getSuccs()){
				/*
				 * Add Loop checking logic
				 */
				BlockVertex src,dest;
				src = getBlockVertexWithId(currentNode, convertedGraph);
				dest = getBlockVertexWithId(succ.getIndexInMethod(), convertedGraph);
				convertedGraph.addEdge(src, dest,new BlockEdge(0));
				if( ! visitedNodes.contains(succ.getIndexInMethod() ) ){
					currentNodes.add(succ.getIndexInMethod());
					
				}
				else
					loopSourceNodes.add(succ.getIndexInMethod());
				
				
			}
			visitedNodes.add(currentNode);	
			
			
		}
		return convertedGraph;
		
	}

	public BlockVertex getVertexWithId(int id,SimpleDirectedWeightedGraph<BlockVertex, BlockEdge> graph){
		
		for(BlockVertex b:graph.vertexSet())
			if(b.getId() == id)
				return b;
		return null;
	}
	
	public void setEdgeWeights(SimpleDirectedWeightedGraph<BlockVertex, BlockEdge> graph){
			TopologicalOrderIterator<BlockVertex, BlockEdge> tI = new TopologicalOrderIterator<>(graph);
			Deque<BlockVertex> stack = new LinkedList<BlockVertex>();
			while(tI.hasNext())
				stack.addLast(tI.next());
			//if tail set lable 1
			while(!stack.isEmpty()){
				BlockVertex currentVertex =  stack.removeLast();
				//if tail
				if(graph.outDegreeOf(currentVertex) == 0)
					currentVertex.setLabel(1);
				else{
					//iterate over all the children and instrument edges
					
					//1.generate set of children
					int parentLabel=0;
					for(BlockEdge childEdge:graph.outgoingEdgesOf(currentVertex)){
						childEdge.setWeight(parentLabel);
						parentLabel=parentLabel+graph.getEdgeTarget(childEdge).getLabel();
					}
					currentVertex.setLabel(parentLabel);
				}
			}
	}
	class Path extends ArrayList<Integer>{}
	
	/**
	 * Data structure 
	 * hashmap of 
	 * @param graph Instrumented Graph 
	 * @return
	 */
	public HashMap<Integer, Path > calculateBLPaths(SimpleDirectedWeightedGraph<BlockVertex,BlockEdge> graph){ 
		
		HashMap<Integer, Path > mapping = new HashMap<Integer, Path >();
		
		ArrayList<ArrayList <Integer> > allPaths = new ArrayList<ArrayList <Integer> >();
		
		HashMap<Integer, ArrayList<Path> > tempPaths= new HashMap<Integer, ArrayList<Path> >();
		TopologicalOrderIterator<BlockVertex, BlockEdge> tI = new TopologicalOrderIterator<>(graph);
		Deque<BlockVertex> stack = new LinkedList<BlockVertex>();
		
		while(tI.hasNext())
			stack.addLast(tI.next());
		
		//for every vertex in reverse topological order
		while(!stack.isEmpty()){
			//if tail
			BlockVertex currentVertex = stack.removeLast();
			if(graph.outDegreeOf(currentVertex)==0){
				Path temp = new Path();
				if(tempPaths.get(currentVertex.getId()) == null)
					tempPaths.put(currentVertex.getId(),new ArrayList<Path>());
				temp.add(currentVertex.getId());
				tempPaths.get(currentVertex.getId()).add(temp);
			}
			else{
				ArrayList<Path> emanatingFrom;
				//for every child of a currentVertex
				for(BlockEdge be:graph.outgoingEdgesOf(currentVertex)){
					
					BlockVertex toSearch = graph.getEdgeTarget(be);
					
					//paths emanating form toSearch
					emanatingFrom = tempPaths.get(toSearch.getId());
					
					//for each path emanating from toSearch
					for(Path a:emanatingFrom){
						//TODO check order
						Path temp = new Path();
						//for each blockid in a path
						for(Integer tocopy : a){
							temp.add(tocopy);
						}
						
						temp.add(currentVertex.getId());
						
						if(!tempPaths.containsKey(currentVertex.getId())){
							tempPaths.put(currentVertex.getId(), new ArrayList<Path>());
						}
						
						(tempPaths.get(currentVertex.getId())).add(temp);
					}
					
				}
				
			}
		}
		for(Integer i:tempPaths.keySet()){
			for(Path p:tempPaths.get(i)){
				Collections.reverse(p);
			}
		}
		
		BlockVertex src=null;
		for(BlockVertex b: graph.vertexSet()){
			if(graph.inDegreeOf(b)==0)
				src = b;
		}
		
		assert(src!=null);
		ArrayList<Path> completePaths = tempPaths.get(src.getId());
		for(Path p:completePaths){
			int id = idForPath(p, graph);
			mapping.put(id, p);
		}
		
		return mapping;		
	}
	int getWeight(SimpleDirectedWeightedGraph<BlockVertex, BlockEdge> graph,
			int src, 
			  int dest){
		
		BlockVertex bvSrc,bvDest;
		bvSrc=getBlockVertexWithId(src, graph);
		bvDest=getBlockVertexWithId(dest, graph);
		for(BlockEdge e:graph.outgoingEdgesOf(bvSrc)){
			if(graph.getEdgeTarget(e).id == dest){
				return e.getWeight();
			}
		}
		return -1;
	}
	
	void instrumentCode(Body b, SimpleDirectedWeightedGraph<BlockVertex,BlockEdge> graph){
		JimpleBody jb = (JimpleBody)b;
		Local id = Jimple.v().newLocal("blPathIdentifier", soot.LongType.v());
		b.getLocals().add(id);
		AssignStmt asStmt = Jimple.v().newAssignStmt(id, LongConstant.v(0));
		b.getUnits().insertBefore(asStmt, jb.getFirstNonIdentityStmt());
		ExceptionalBlockGraph cfg = new ExceptionalBlockGraph(b);
		//for each block
		for(Block bl : cfg.getBlocks()){
			//for each successor
			for(Block succ:bl.getSuccs()){
				//get edgeweight from graph
				//add that instrumentation
				int toAdd = getWeight(graph, bl.getIndexInMethod(), succ.getIndexInMethod());
				AddExpr ae = Jimple.v().newAddExpr(id, LongConstant.v(toAdd));
				AssignStmt toInsert = Jimple.v().newAssignStmt(id, ae);
				b.getUnits().insertOnEdge(toInsert, bl.getTail(), succ.getHead());
			}
		}
		
		if(b.getMethod().getName().equals("main")){
			Unit lastUnit = b.getUnits().getLast();
			SootClass streamClass = Scene.v().getSootClass("java.io.PrintStream");
			SootMethod sopln = streamClass.getMethod("void print(long)");
			streamInstance = Jimple.v().newLocal("StreamInstance", RefType.v("java.io.PrintStream"));
			b.getLocals().add(streamInstance);
			b.getUnits().insertBefore(Jimple.v().newAssignStmt(streamInstance, Jimple.v().newStaticFieldRef(Scene.v()
					.getField("<java.lang.System: java.io.PrintStream out>").makeRef()))
					,lastUnit);
			InvokeExpr ie = Jimple.v().newVirtualInvokeExpr(streamInstance, sopln.makeRef(),id);
			InvokeStmt is = Jimple.v().newInvokeStmt(ie);
			b.getUnits().insertBefore(is, lastUnit);
		}
		
		System.out.println(b.toString());
		/* 0. for every block
		 * 1. Get last if
		 * 2. check targetstmt with first of all the 
		 * 3. 
		 * 
		 */
		
	}
	
	public int idForPath(Path p, SimpleDirectedWeightedGraph<BlockVertex, BlockEdge> graph){
		int id=0;
		int i=0;
		for(Integer vertexId:p){
			BlockVertex thisVertex = getVertexWithId(vertexId, graph);
			for(BlockEdge e:graph.outgoingEdgesOf(thisVertex)){
				if(graph.getEdgeTarget(e).getId() == p.get(i+1)){
					id=id+e.getWeight();
					break;
				}
			}
			if(i==p.size()-2){
				break;
			}
			else
				i++;
			
		}
		return id;
	}
	protected void internalTransform(Body b, String phaseName, Map<String, String> options) {
		ExceptionalBlockGraph cfg = new ExceptionalBlockGraph(b);
//		System.out.println("cfg");
//		System.out.println(cfg.toString());
//		
		System.out.println("converted");
		System.out.println(convertSootToJGraphT(cfg).toString());
		
		SimpleDirectedWeightedGraph<BlockVertex, BlockEdge> callGraph = convertSootToJGraphT(cfg);
		setEdgeWeights(callGraph);
		
		System.out.println("calculated");
		System.out.println(callGraph);
		System.out.println(cfg.toString());
		
		calculateBLPaths(callGraph);
		
		synchronized(this){
			instrumentCode(b, callGraph);
		}
		
		
		//storeTheMapping()
		
		/*
		 * 1. Convert Soot graph to jgrapht
		 * 2. Get Topological Sort 
		 * 3. Ball Larus
		 */
		
		
	
	}
	
}