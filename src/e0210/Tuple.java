package e0210;

import java.util.ArrayList;
import java.util.List;

public class Tuple{
	String methodSignature, threadId;
	long previouslyCalled, blId;
	ProcessOutput po;
	ArrayList<Integer> path;
	public Tuple() {
		methodSignature = new String("");
		threadId= new String("");
		previouslyCalled = 0;
		blId = 0;
		po=new ProcessOutput();
	}
	
	public Tuple(Tuple t){
		methodSignature = new String(t.methodSignature);
		threadId = new String(t.threadId);
		previouslyCalled = t.previouslyCalled;
		blId = t.previouslyCalled;
		po = new ProcessOutput();
		path=new ArrayList<Integer>();
		for(Integer i : t.path){
			path.add(new Integer(i));
		}
	}
	
	public Tuple(String sig, String thid, long pc,long blid) {
		methodSignature = new String(sig);
		threadId= new String(thid);
		previouslyCalled = pc;
		blId = blid;
		po=new ProcessOutput();
		path = po.decodeBLId(blid, sig);
	}
	public String toString(){
		String toRet = methodSignature+"|"+threadId+"|"+Long.toString(previouslyCalled)+
				"|"+path.toString();
		return toRet;
	}
}