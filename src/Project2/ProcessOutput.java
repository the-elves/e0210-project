package e0210.Project2;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/*
 * @author Sridhar Gopinath		-		g.sridhar53@gmail.com
 * 
 * Course project,
 * Principles of Programming Course, Fall - 2016,
 * Computer Science and Automation (CSA),
 * Indian Institute of Science (IISc),
 * Bangalore
 */

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ProcessOutput {
	class Path extends ArrayList<Integer>{};
	HashMap<String, List< HashMap<Integer,Path> > > methodData;
	List< HashMap<Integer,Path> > mapTuple;
	
	public ProcessOutput() {
		// TODO Auto-generated constructor stub
		methodData = new HashMap<String, List< HashMap<Integer,Path> > >();
	}
	int getId(String line){
		return new Integer(line.split(":")[0]);
	}
	Path getPath(String line){
		Path path = new Path();
		for (String i:line.split(":")[1].split("  ")){
			if(new Integer(i) >= 0)
			path.add(new Integer(i));
		}
		return path;
	}
	
	void buildHashMaps(){
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("sootOutput/mapping.ser")));
			String line;
			String signature=new String(""); 
			while((line = br.readLine())!=null){
				if(line.equals("MethodStart")){
					signature = br.readLine();
					mapTuple = new ArrayList<HashMap<Integer,Path> >();
				}
				else if(line.equals("LoopPathStart")){
					HashMap<Integer,Path> loopPaths = new HashMap<Integer,Path>(); 
					while(! ( (line = br.readLine() ).equals("LoopPathEnd") ) ){
						int id = getId(line);
						Path path = getPath(line);
						loopPaths.put(id, path);
					}
					mapTuple.add(0,loopPaths);
						
				}
				else if(line.equals("WholePathStart")){
					HashMap<Integer,Path> wholePaths = new HashMap<Integer,Path>(); 
					while(! ( (line = br.readLine() ).equals("WholePathEnd") ) ){
						int id = getId(line);
						Path path = getPath(line);
						wholePaths.put(id, path);
					}
					mapTuple.add(1,wholePaths);
				}
				else if(line.equals("MethodEnd")){
					methodData.put(signature, mapTuple);
				}
					
			}
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	String decodeBLId(String in){
		BufferedReader br = new BufferedReader(new StringReader(in));
		String line;
		Integer BLId;
		String currentMethod =new String("");
		String previousMethod=new String("");
		String result = new String ("");
		try {
			line = br.readLine();
			if(line != null){
				BLId= new Integer(line);
				for(String sig:methodData.keySet()){
					for(HashMap<Integer,Path> h : methodData.get(sig)){
						if(h.containsKey(BLId)){
							previousMethod =new String(sig);
							Path p = h.get(BLId);
							int i ;
							for( i = 0; i < p.size()-1;i++){
	
								result=result+p.get(i).toString()+"\n";
							}
	
							result=result+p.get(i).toString();

						}
					}
				}
			}
			
			
			while( (line = br.readLine()) != null &&!line.equals("") ){
				result=result+"\n";
				BLId= new Integer(line);
				for(String sig:methodData.keySet()){
					for(HashMap<Integer,Path> h : methodData.get(sig)){
						if(h.containsKey(BLId)){
							
							currentMethod =new String(sig);
							Path p = h.get(BLId);
							int i ;
							
							if(methodData.get(sig).indexOf(h)==1 || !currentMethod.equals(previousMethod)){
								result = result +"\n";
								
							}
							
							for( i = 0; i < p.size()-1;i++){
								result=result+p.get(i).toString() + "\n";
							}
							result=result + p.get(i).toString();
							
							previousMethod = new String(currentMethod);
							break;
						}
					}
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	public static void main(String[] args) throws IOException {
		String project = args[0];
		String testcase = args[1];

		String inPath = "Testcases/" + project + "/output/" + testcase;
		String outPath = "Testcases/" + project + "/processed-output/" + testcase;

		System.out.println("Processing " + testcase + " of " + project);
		ProcessOutput po = new ProcessOutput();
		po.buildHashMaps();
		
		// Read the contents of the output file into a string
		String in = new String(Files.readAllBytes(Paths.get(inPath)));
		String result = po.decodeBLId(in);
		System.out.println(result);
		/*
		 * 
		 * Write your algorithm which does the post-processing of the output
		 * 
		 */
		 
		// Write the contents of the string to the output file
		PrintWriter out = new PrintWriter(outPath);
		out.print(result);

		out.close();

		return;
	}

}